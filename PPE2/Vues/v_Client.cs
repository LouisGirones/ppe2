﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PPE2
{
    public partial class v_Client : Form
    {
        public v_Client()
        {
            InitializeComponent();
        }

        private void v_Client_Load(object sender, EventArgs e)
        {
            //Récupération des clients
            listeBoxCli.DataSource = null;
            listeBoxCli.DataSource = ClientDAO.GetClients();

            //Sélection du sexe du client sélectionné
            if (listeBoxCli.SelectedItem != null)
            {
                Client c = (Client)listeBoxCli.SelectedItem;
                SetRbSexe(c.Sexe);
            }
        }

        private void btAjouterClient_Click(object sender, EventArgs e)
        {
           if ( ! Verification.IsEmpty(txtLibPrenom.Text) && ! Verification.IsEmpty(txtLibNom.Text) && Verification.SelectedRb(this))
            {
               //recup de la valeur du tag du radioButton du sexe selectionné
                string sexe = this.Controls.OfType<RadioButton>().
                    Where(r => r.Checked == true).FirstOrDefault().Tag.ToString();
               //Le client n'existe pas encore
                if (!ClientDAO.ExisteClient(txtLibNom.Text, txtLibPrenom.Text, sexe))
                {
                    Client c;
                    c = new Client(txtLibNom.Text, txtLibPrenom.Text, sexe);
                    ClientDAO.InsererClient(c);
                    //Actualisation de la liste de clients
                    listeBoxCli.DataSource = null;
                    listeBoxCli.DataSource = ClientDAO.GetClients();
                }
                else
                {
                    MessageBox.Show(Outils.CiviliteClient(sexe, "Ce client", "Cette cliente")+" existe déjà.");
                }
            }
            else
            {
                MessageBox.Show("Vous devez entrer le nom, le prénom et le sexe du client.");
            }
        }

        private void btSupprimerClient_Click(object sender, EventArgs e) 
        {
           if (listeBoxCli.SelectedItem != null)
            {
               Client c = (Client)listeBoxCli.SelectedItem;
               if (Outils.ConfirmationModal("supprimer "+Outils.CiviliteClient(c.Sexe, "ce client", "cette cliente")+" ?"))
               {            
                   ClientDAO.SupprimerClient(c);
                   //Le client a été supprimé, actualisation de la liste affichée
                   listeBoxCli.DataSource = null;
                   listeBoxCli.DataSource = ClientDAO.GetClients();
               }
            }
            else
            {
                MessageBox.Show("Vous devez selectionner un client.");
            }
        }

        /// <summary>
        /// Passage des champs du client sélectionné dans les textboxs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listeBoxCli_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listeBoxCli.SelectedItem != null)
            {
                Client c = (Client)listeBoxCli.SelectedItem;
                txtLibPrenom.Text = c.Prenom;
                txtLibNom.Text = c.Nom;
                SetRbSexe(c.Sexe);
            }
        }

        /// <summary>
        /// Vider les champs
        /// Textboxs et radio bouton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btVider_Click(object sender, EventArgs e)
        {
            Outils.ViderTextBoxs(this);
            listeBoxCli.SelectedItem = null;
            rbFemme.Checked = false;
            rbHomme.Checked = false;
        }

        /// <summary>
        /// Selectionne le radio bouton correspondant au string passé en param
        /// </summary>
        /// <param name="s">Homme ou Femme</param>
        private void SetRbSexe(string s)
        {
            if (s == "Homme")
            {
                rbHomme.Checked = true;
            }
            else if (s == "Femme")
            {
                rbFemme.Checked = true;
            }
        }

        #region SwitchForms
        private void btActivite_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Act.Show();
        }

        private void btEquipement_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Eq.Show();
        }

        private void btPlanning_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Plan.Show();
        }
        #endregion

        private void v_Client_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
