﻿namespace PPE2
{
    partial class v_Planning
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_Equipement = new System.Windows.Forms.Button();
            this.btUser = new System.Windows.Forms.Button();
            this.btAct = new System.Windows.Forms.Button();
            this.dtgPlanning = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpSemaines = new System.Windows.Forms.DateTimePicker();
            this.btModifPlanning = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPlanning)).BeginInit();
            this.SuspendLayout();
            // 
            // bt_Equipement
            // 
            this.bt_Equipement.Location = new System.Drawing.Point(33, 26);
            this.bt_Equipement.Margin = new System.Windows.Forms.Padding(4);
            this.bt_Equipement.Name = "bt_Equipement";
            this.bt_Equipement.Size = new System.Drawing.Size(117, 28);
            this.bt_Equipement.TabIndex = 39;
            this.bt_Equipement.Text = "Equipements";
            this.bt_Equipement.UseVisualStyleBackColor = true;
            this.bt_Equipement.Click += new System.EventHandler(this.bt_Equipement_Click);
            // 
            // btUser
            // 
            this.btUser.Location = new System.Drawing.Point(228, 26);
            this.btUser.Margin = new System.Windows.Forms.Padding(4);
            this.btUser.Name = "btUser";
            this.btUser.Size = new System.Drawing.Size(117, 28);
            this.btUser.TabIndex = 44;
            this.btUser.Text = "Clients";
            this.btUser.UseVisualStyleBackColor = true;
            this.btUser.Click += new System.EventHandler(this.btUser_Click);
            // 
            // btAct
            // 
            this.btAct.Location = new System.Drawing.Point(393, 26);
            this.btAct.Margin = new System.Windows.Forms.Padding(4);
            this.btAct.Name = "btAct";
            this.btAct.Size = new System.Drawing.Size(117, 28);
            this.btAct.TabIndex = 45;
            this.btAct.Text = "Activités";
            this.btAct.UseVisualStyleBackColor = true;
            this.btAct.Click += new System.EventHandler(this.btAct_Click);
            // 
            // dtgPlanning
            // 
            this.dtgPlanning.AllowUserToAddRows = false;
            this.dtgPlanning.AllowUserToDeleteRows = false;
            this.dtgPlanning.AllowUserToResizeColumns = false;
            this.dtgPlanning.AllowUserToResizeRows = false;
            this.dtgPlanning.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPlanning.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgPlanning.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtgPlanning.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dtgPlanning.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPlanning.Location = new System.Drawing.Point(33, 112);
            this.dtgPlanning.Margin = new System.Windows.Forms.Padding(4);
            this.dtgPlanning.MultiSelect = false;
            this.dtgPlanning.Name = "dtgPlanning";
            this.dtgPlanning.ReadOnly = true;
            this.dtgPlanning.RowTemplate.Height = 40;
            this.dtgPlanning.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dtgPlanning.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dtgPlanning.ShowEditingIcon = false;
            this.dtgPlanning.Size = new System.Drawing.Size(1427, 621);
            this.dtgPlanning.TabIndex = 47;
            this.dtgPlanning.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgPlanning_CellDoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 76);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(325, 34);
            this.label2.TabIndex = 49;
            this.label2.Text = "Voir / modifier le planning de la semaine :\r\n(La date sera celle du lundi de la s" +
                "emaine choisie)\r\n";
            // 
            // dtpSemaines
            // 
            this.dtpSemaines.Location = new System.Drawing.Point(301, 69);
            this.dtpSemaines.Margin = new System.Windows.Forms.Padding(4);
            this.dtpSemaines.Name = "dtpSemaines";
            this.dtpSemaines.Size = new System.Drawing.Size(265, 22);
            this.dtpSemaines.TabIndex = 50;
            this.dtpSemaines.Value = new System.DateTime(2016, 5, 2, 17, 48, 12, 0);
            this.dtpSemaines.ValueChanged += new System.EventHandler(this.dtpSemaines_ValueChanged);
            // 
            // btModifPlanning
            // 
            this.btModifPlanning.Location = new System.Drawing.Point(32, 784);
            this.btModifPlanning.Margin = new System.Windows.Forms.Padding(4);
            this.btModifPlanning.Name = "btModifPlanning";
            this.btModifPlanning.Size = new System.Drawing.Size(164, 28);
            this.btModifPlanning.TabIndex = 51;
            this.btModifPlanning.Text = "Gérer le planning";
            this.btModifPlanning.UseVisualStyleBackColor = true;
            this.btModifPlanning.Click += new System.EventHandler(this.btModifPlanning_Click);
            // 
            // v_Planning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1527, 842);
            this.Controls.Add(this.btModifPlanning);
            this.Controls.Add(this.dtpSemaines);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtgPlanning);
            this.Controls.Add(this.btAct);
            this.Controls.Add(this.btUser);
            this.Controls.Add(this.bt_Equipement);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "v_Planning";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Planning";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.v_Activite_FormClosing);
            this.Load += new System.EventHandler(this.v_Planning_Load);
            this.VisibleChanged += new System.EventHandler(this.v_Planning_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPlanning)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_Equipement;
        private System.Windows.Forms.Button btUser;
        private System.Windows.Forms.Button btAct;
        private System.Windows.Forms.DataGridView dtgPlanning;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpSemaines;
        private System.Windows.Forms.Button btModifPlanning;
    }
}

