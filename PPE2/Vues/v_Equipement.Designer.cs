﻿namespace PPE2
{
    partial class v_Equipement
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDescrEq = new System.Windows.Forms.TextBox();
            this.txtLibEq = new System.Windows.Forms.TextBox();
            this.btAjouterMat = new System.Windows.Forms.Button();
            this.libNbEq = new System.Windows.Forms.Label();
            this.descrEq = new System.Windows.Forms.Label();
            this.libEq = new System.Windows.Forms.Label();
            this.btSupprimerMat = new System.Windows.Forms.Button();
            this.btModifierMat = new System.Windows.Forms.Button();
            this.listeBoxEq = new System.Windows.Forms.ListBox();
            this.btActivite = new System.Windows.Forms.Button();
            this.btClients = new System.Windows.Forms.Button();
            this.btVider = new System.Windows.Forms.Button();
            this.btPlanning = new System.Windows.Forms.Button();
            this.nbEq = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nbEq)).BeginInit();
            this.SuspendLayout();
            // 
            // txtDescrEq
            // 
            this.txtDescrEq.Location = new System.Drawing.Point(531, 120);
            this.txtDescrEq.MaxLength = 200;
            this.txtDescrEq.Multiline = true;
            this.txtDescrEq.Name = "txtDescrEq";
            this.txtDescrEq.Size = new System.Drawing.Size(261, 138);
            this.txtDescrEq.TabIndex = 37;
            // 
            // txtLibEq
            // 
            this.txtLibEq.Location = new System.Drawing.Point(531, 80);
            this.txtLibEq.MaxLength = 32;
            this.txtLibEq.Name = "txtLibEq";
            this.txtLibEq.Size = new System.Drawing.Size(121, 20);
            this.txtLibEq.TabIndex = 36;
            // 
            // btAjouterMat
            // 
            this.btAjouterMat.Location = new System.Drawing.Point(535, 353);
            this.btAjouterMat.Name = "btAjouterMat";
            this.btAjouterMat.Size = new System.Drawing.Size(82, 49);
            this.btAjouterMat.TabIndex = 35;
            this.btAjouterMat.Text = "Ajouter";
            this.btAjouterMat.UseVisualStyleBackColor = true;
            this.btAjouterMat.Click += new System.EventHandler(this.btAjouterEq_Click);
            // 
            // libNbEq
            // 
            this.libNbEq.AutoSize = true;
            this.libNbEq.Location = new System.Drawing.Point(451, 267);
            this.libNbEq.Name = "libNbEq";
            this.libNbEq.Size = new System.Drawing.Size(50, 13);
            this.libNbEq.TabIndex = 34;
            this.libNbEq.Text = "Nombre :";
            // 
            // descrEq
            // 
            this.descrEq.AutoSize = true;
            this.descrEq.Location = new System.Drawing.Point(451, 120);
            this.descrEq.Name = "descrEq";
            this.descrEq.Size = new System.Drawing.Size(66, 13);
            this.descrEq.TabIndex = 33;
            this.descrEq.Text = "Description :";
            // 
            // libEq
            // 
            this.libEq.AutoSize = true;
            this.libEq.Location = new System.Drawing.Point(451, 83);
            this.libEq.Name = "libEq";
            this.libEq.Size = new System.Drawing.Size(37, 13);
            this.libEq.TabIndex = 32;
            this.libEq.Text = "Libellé";
            // 
            // btSupprimerMat
            // 
            this.btSupprimerMat.Location = new System.Drawing.Point(367, 361);
            this.btSupprimerMat.Name = "btSupprimerMat";
            this.btSupprimerMat.Size = new System.Drawing.Size(87, 41);
            this.btSupprimerMat.TabIndex = 29;
            this.btSupprimerMat.Text = "Supprimer";
            this.btSupprimerMat.UseVisualStyleBackColor = true;
            this.btSupprimerMat.Click += new System.EventHandler(this.btSupprimerEq_Click);
            // 
            // btModifierMat
            // 
            this.btModifierMat.Location = new System.Drawing.Point(664, 348);
            this.btModifierMat.Name = "btModifierMat";
            this.btModifierMat.Size = new System.Drawing.Size(93, 54);
            this.btModifierMat.TabIndex = 28;
            this.btModifierMat.Text = "Modifier";
            this.btModifierMat.UseVisualStyleBackColor = true;
            this.btModifierMat.Click += new System.EventHandler(this.btModifierEq_Click);
            // 
            // listeBoxEq
            // 
            this.listeBoxEq.FormattingEnabled = true;
            this.listeBoxEq.Location = new System.Drawing.Point(52, 74);
            this.listeBoxEq.Name = "listeBoxEq";
            this.listeBoxEq.Size = new System.Drawing.Size(239, 238);
            this.listeBoxEq.TabIndex = 26;
            this.listeBoxEq.SelectedIndexChanged += new System.EventHandler(this.listeBoxEq_SelectedIndexChanged);
            // 
            // btActivite
            // 
            this.btActivite.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btActivite.Location = new System.Drawing.Point(29, 13);
            this.btActivite.Name = "btActivite";
            this.btActivite.Size = new System.Drawing.Size(75, 23);
            this.btActivite.TabIndex = 39;
            this.btActivite.Text = "Activités";
            this.btActivite.UseVisualStyleBackColor = true;
            this.btActivite.Click += new System.EventHandler(this.btActivite_Click);
            // 
            // btClients
            // 
            this.btClients.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btClients.Location = new System.Drawing.Point(145, 13);
            this.btClients.Name = "btClients";
            this.btClients.Size = new System.Drawing.Size(75, 23);
            this.btClients.TabIndex = 40;
            this.btClients.Text = "Clients";
            this.btClients.UseVisualStyleBackColor = true;
            this.btClients.Click += new System.EventHandler(this.btClients_Click);
            // 
            // btVider
            // 
            this.btVider.Location = new System.Drawing.Point(705, 59);
            this.btVider.Name = "btVider";
            this.btVider.Size = new System.Drawing.Size(87, 41);
            this.btVider.TabIndex = 49;
            this.btVider.Text = "Vider les champs";
            this.btVider.UseVisualStyleBackColor = true;
            this.btVider.Click += new System.EventHandler(this.btVider_Click);
            // 
            // btPlanning
            // 
            this.btPlanning.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btPlanning.Location = new System.Drawing.Point(278, 13);
            this.btPlanning.Name = "btPlanning";
            this.btPlanning.Size = new System.Drawing.Size(75, 23);
            this.btPlanning.TabIndex = 50;
            this.btPlanning.Text = "Planning";
            this.btPlanning.UseVisualStyleBackColor = true;
            this.btPlanning.Click += new System.EventHandler(this.btPlanning_Click);
            // 
            // nbEq
            // 
            this.nbEq.Location = new System.Drawing.Point(535, 267);
            this.nbEq.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nbEq.Name = "nbEq";
            this.nbEq.Size = new System.Drawing.Size(120, 20);
            this.nbEq.TabIndex = 51;
            // 
            // v_Equipement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 463);
            this.Controls.Add(this.nbEq);
            this.Controls.Add(this.btPlanning);
            this.Controls.Add(this.btVider);
            this.Controls.Add(this.btClients);
            this.Controls.Add(this.btActivite);
            this.Controls.Add(this.txtDescrEq);
            this.Controls.Add(this.txtLibEq);
            this.Controls.Add(this.btAjouterMat);
            this.Controls.Add(this.libNbEq);
            this.Controls.Add(this.descrEq);
            this.Controls.Add(this.libEq);
            this.Controls.Add(this.btSupprimerMat);
            this.Controls.Add(this.btModifierMat);
            this.Controls.Add(this.listeBoxEq);
            this.Name = "v_Equipement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Equipements";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.v_Equipement_FormClosing);
            this.Load += new System.EventHandler(this.v_Equipement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nbEq)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDescrEq;
        private System.Windows.Forms.TextBox txtLibEq;
        private System.Windows.Forms.Button btAjouterMat;
        private System.Windows.Forms.Label libNbEq;
        private System.Windows.Forms.Label descrEq;
        private System.Windows.Forms.Label libEq;
        private System.Windows.Forms.Button btSupprimerMat;
        private System.Windows.Forms.Button btModifierMat;
        private System.Windows.Forms.ListBox listeBoxEq;
        private System.Windows.Forms.Button btActivite;
        private System.Windows.Forms.Button btClients;
        private System.Windows.Forms.Button btVider;
        private System.Windows.Forms.Button btPlanning;
        private System.Windows.Forms.NumericUpDown nbEq;
    }
}

