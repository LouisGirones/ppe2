﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace PPE2
{
    public partial class v_Planning : Form
    {

        public v_Planning()
        {
            InitializeComponent();
        }


        private void v_Planning_Load(object sender, EventArgs e)
        {
            dtpSemaines.Value = DateTime.Now;
        }

        private void dtpSemaines_ValueChanged(object sender, EventArgs e)
        {
            SetDatepickerToMonday(dtpSemaines.Value);
            //Initialisation du datagridview
            OutilsAffichagePlanning.Initialize(dtgPlanning, PlanningDAO.GetPlanning(dtpSemaines.Value.Date.ToString("yyyy-MM-dd"), dtpSemaines.Value.Add(new TimeSpan(5, 23, 59, 59)).Date.ToString("yyyy-MM-dd")), dtpSemaines.Value);
            
        }

        private void SetDatepickerToMonday(DateTime dtpDate)
        {
            if (dtpDate.DayOfWeek != DayOfWeek.Monday)
            {
                var offset = (int)DayOfWeek.Monday - (int)dtpDate.DayOfWeek;
                var monday = dtpDate + TimeSpan.FromDays(offset);
                dtpSemaines.Value = monday;
            }
        }

        private void btModifPlanning_Click(object sender, EventArgs e)
        {
            this.Hide();
            v_GestPlanning gest = new v_GestPlanning(dtpSemaines.Value);
            DialogResult dr = gest.ShowDialog();
            
            if (dr == DialogResult.Cancel)
            {
                this.Show();
            }
        }

        #region SwitchForms
        private void bt_Equipement_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Eq.Show();
        }

        private void btUser_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Cli.Show();
        }

        private void btAct_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Act.Show();
        }

        #endregion

        private void v_Activite_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void dtgPlanning_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Le click n'est pas sur les horaires ou les noms des jours
            if (e.ColumnIndex != 0 && e.RowIndex != -1)
            {
                MessageBox.Show(dtgPlanning.Columns[e.ColumnIndex].Name+ " "+dtgPlanning[0, e.RowIndex].Value.ToString());
            }
        }

        private void v_Planning_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                SetDatepickerToMonday(dtpSemaines.Value);
                OutilsAffichagePlanning.Initialize(dtgPlanning, PlanningDAO.GetPlanning(dtpSemaines.Value.Date.ToString("yyyy-MM-dd"), dtpSemaines.Value.Add(new TimeSpan(5, 23, 59, 59)).Date.ToString("yyyy-MM-dd")), dtpSemaines.Value);
            }
        }

    }
}
