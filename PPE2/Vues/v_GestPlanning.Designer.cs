﻿namespace PPE2
{
    partial class v_GestPlanning
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btAjouterAct = new System.Windows.Forms.Button();
            this.btSupprimerAct = new System.Windows.Forms.Button();
            this.btModifierAct = new System.Windows.Forms.Button();
            this.listeBoxPlanning = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbAct = new System.Windows.Forms.ComboBox();
            this.btRetour = new System.Windows.Forms.Button();
            this.lbWeek = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbDuree = new System.Windows.Forms.Label();
            this.lbPart = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbJours = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbHeure = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btAjoutClient = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btAjouterAct
            // 
            this.btAjouterAct.Location = new System.Drawing.Point(751, 434);
            this.btAjouterAct.Margin = new System.Windows.Forms.Padding(4);
            this.btAjouterAct.Name = "btAjouterAct";
            this.btAjouterAct.Size = new System.Drawing.Size(109, 60);
            this.btAjouterAct.TabIndex = 35;
            this.btAjouterAct.Text = "Ajouter";
            this.btAjouterAct.UseVisualStyleBackColor = true;
            this.btAjouterAct.Click += new System.EventHandler(this.btAjouterPlanning_Click);
            // 
            // btSupprimerAct
            // 
            this.btSupprimerAct.Location = new System.Drawing.Point(588, 439);
            this.btSupprimerAct.Margin = new System.Windows.Forms.Padding(4);
            this.btSupprimerAct.Name = "btSupprimerAct";
            this.btSupprimerAct.Size = new System.Drawing.Size(116, 50);
            this.btSupprimerAct.TabIndex = 29;
            this.btSupprimerAct.Text = "Supprimer";
            this.btSupprimerAct.UseVisualStyleBackColor = true;
            this.btSupprimerAct.Click += new System.EventHandler(this.btSupprimerPlanning_Click);
            // 
            // btModifierAct
            // 
            this.btModifierAct.Location = new System.Drawing.Point(919, 428);
            this.btModifierAct.Margin = new System.Windows.Forms.Padding(4);
            this.btModifierAct.Name = "btModifierAct";
            this.btModifierAct.Size = new System.Drawing.Size(124, 66);
            this.btModifierAct.TabIndex = 28;
            this.btModifierAct.Text = "Modifier";
            this.btModifierAct.UseVisualStyleBackColor = true;
            this.btModifierAct.Click += new System.EventHandler(this.btModifierPlanning_Click);
            // 
            // listeBoxPlanning
            // 
            this.listeBoxPlanning.FormattingEnabled = true;
            this.listeBoxPlanning.ItemHeight = 16;
            this.listeBoxPlanning.Location = new System.Drawing.Point(120, 91);
            this.listeBoxPlanning.Margin = new System.Windows.Forms.Padding(4);
            this.listeBoxPlanning.Name = "listeBoxPlanning";
            this.listeBoxPlanning.Size = new System.Drawing.Size(317, 292);
            this.listeBoxPlanning.TabIndex = 26;
            this.listeBoxPlanning.Click += new System.EventHandler(this.listeBoxPlanning_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(604, 116);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 17);
            this.label1.TabIndex = 42;
            this.label1.Text = "Activité";
            // 
            // cbAct
            // 
            this.cbAct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAct.FormattingEnabled = true;
            this.cbAct.Location = new System.Drawing.Point(713, 112);
            this.cbAct.Margin = new System.Windows.Forms.Padding(4);
            this.cbAct.Name = "cbAct";
            this.cbAct.Size = new System.Drawing.Size(177, 24);
            this.cbAct.TabIndex = 43;
            this.cbAct.SelectedIndexChanged += new System.EventHandler(this.cbAct_SelectedIndexChanged);
            // 
            // btRetour
            // 
            this.btRetour.Location = new System.Drawing.Point(89, 444);
            this.btRetour.Margin = new System.Windows.Forms.Padding(4);
            this.btRetour.Name = "btRetour";
            this.btRetour.Size = new System.Drawing.Size(116, 50);
            this.btRetour.TabIndex = 48;
            this.btRetour.Text = "Retour";
            this.btRetour.UseVisualStyleBackColor = true;
            this.btRetour.Click += new System.EventHandler(this.btRetour_Click);
            // 
            // lbWeek
            // 
            this.lbWeek.AutoSize = true;
            this.lbWeek.Location = new System.Drawing.Point(357, 27);
            this.lbWeek.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbWeek.Name = "lbWeek";
            this.lbWeek.Size = new System.Drawing.Size(55, 17);
            this.lbWeek.TabIndex = 52;
            this.lbWeek.Text = "lbWeek";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(604, 167);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 17);
            this.label2.TabIndex = 53;
            this.label2.Text = "Durée ";
            // 
            // lbDuree
            // 
            this.lbDuree.AutoSize = true;
            this.lbDuree.Location = new System.Drawing.Point(664, 167);
            this.lbDuree.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbDuree.Name = "lbDuree";
            this.lbDuree.Size = new System.Drawing.Size(58, 17);
            this.lbDuree.TabIndex = 54;
            this.lbDuree.Text = "lbDuree";
            // 
            // lbPart
            // 
            this.lbPart.AutoSize = true;
            this.lbPart.Location = new System.Drawing.Point(880, 167);
            this.lbPart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPart.Name = "lbPart";
            this.lbPart.Size = new System.Drawing.Size(45, 17);
            this.lbPart.TabIndex = 56;
            this.lbPart.Text = "lbPart";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(736, 167);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 17);
            this.label5.TabIndex = 55;
            this.label5.Text = "Nb participants Max.";
            // 
            // cbJours
            // 
            this.cbJours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbJours.FormattingEnabled = true;
            this.cbJours.Location = new System.Drawing.Point(713, 223);
            this.cbJours.Margin = new System.Windows.Forms.Padding(4);
            this.cbJours.Name = "cbJours";
            this.cbJours.Size = new System.Drawing.Size(177, 24);
            this.cbJours.TabIndex = 58;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(604, 226);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 17);
            this.label3.TabIndex = 57;
            this.label3.Text = "Jour";
            // 
            // cbHeure
            // 
            this.cbHeure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHeure.FormattingEnabled = true;
            this.cbHeure.Location = new System.Drawing.Point(713, 298);
            this.cbHeure.Margin = new System.Windows.Forms.Padding(4);
            this.cbHeure.Name = "cbHeure";
            this.cbHeure.Size = new System.Drawing.Size(177, 24);
            this.cbHeure.TabIndex = 60;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(597, 302);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 17);
            this.label4.TabIndex = 59;
            this.label4.Text = "Heure de début";
            // 
            // btAjoutClient
            // 
            this.btAjoutClient.Location = new System.Drawing.Point(295, 428);
            this.btAjoutClient.Margin = new System.Windows.Forms.Padding(4);
            this.btAjoutClient.Name = "btAjoutClient";
            this.btAjoutClient.Size = new System.Drawing.Size(142, 50);
            this.btAjoutClient.TabIndex = 61;
            this.btAjoutClient.Text = "Ajouter des clients\r\n à l\'activité";
            this.btAjoutClient.UseVisualStyleBackColor = true;
            this.btAjoutClient.Click += new System.EventHandler(this.btAjoutClient_Click);
            // 
            // v_GestPlanning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1140, 570);
            this.Controls.Add(this.btAjoutClient);
            this.Controls.Add(this.cbHeure);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbJours);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbPart);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbDuree);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbWeek);
            this.Controls.Add(this.btRetour);
            this.Controls.Add(this.cbAct);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btAjouterAct);
            this.Controls.Add(this.btSupprimerAct);
            this.Controls.Add(this.btModifierAct);
            this.Controls.Add(this.listeBoxPlanning);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "v_GestPlanning";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion du planning";
            this.Load += new System.EventHandler(this.v_GestPlanning_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btAjouterAct;
        private System.Windows.Forms.Button btSupprimerAct;
        private System.Windows.Forms.Button btModifierAct;
        private System.Windows.Forms.ListBox listeBoxPlanning;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbAct;
        private System.Windows.Forms.Button btRetour;
        private System.Windows.Forms.Label lbWeek;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbDuree;
        private System.Windows.Forms.Label lbPart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbJours;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbHeure;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btAjoutClient;
    }
}

