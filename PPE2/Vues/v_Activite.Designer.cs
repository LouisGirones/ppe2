﻿namespace PPE2
{
    partial class v_Activite
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDescrAct = new System.Windows.Forms.TextBox();
            this.txtLibAct = new System.Windows.Forms.TextBox();
            this.btAjouterAct = new System.Windows.Forms.Button();
            this.descrAct = new System.Windows.Forms.Label();
            this.libAct = new System.Windows.Forms.Label();
            this.btSupprimerAct = new System.Windows.Forms.Button();
            this.btModifierAct = new System.Windows.Forms.Button();
            this.listeBoxAct = new System.Windows.Forms.ListBox();
            this.bt_Equipement = new System.Windows.Forms.Button();
            this.lbDuree = new System.Windows.Forms.Label();
            this.btUser = new System.Windows.Forms.Button();
            this.libClientMax = new System.Windows.Forms.Label();
            this.nbMaxPart = new System.Windows.Forms.NumericUpDown();
            this.btVider = new System.Windows.Forms.Button();
            this.btPlanning = new System.Windows.Forms.Button();
            this.cbDuree = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbEq = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.nbMaxPart)).BeginInit();
            this.SuspendLayout();
            // 
            // txtDescrAct
            // 
            this.txtDescrAct.Location = new System.Drawing.Point(511, 180);
            this.txtDescrAct.MaxLength = 200;
            this.txtDescrAct.Multiline = true;
            this.txtDescrAct.Name = "txtDescrAct";
            this.txtDescrAct.Size = new System.Drawing.Size(261, 113);
            this.txtDescrAct.TabIndex = 37;
            // 
            // txtLibAct
            // 
            this.txtLibAct.Location = new System.Drawing.Point(531, 80);
            this.txtLibAct.MaxLength = 32;
            this.txtLibAct.Name = "txtLibAct";
            this.txtLibAct.Size = new System.Drawing.Size(121, 20);
            this.txtLibAct.TabIndex = 36;
            // 
            // btAjouterAct
            // 
            this.btAjouterAct.Location = new System.Drawing.Point(535, 353);
            this.btAjouterAct.Name = "btAjouterAct";
            this.btAjouterAct.Size = new System.Drawing.Size(82, 49);
            this.btAjouterAct.TabIndex = 35;
            this.btAjouterAct.Text = "Ajouter";
            this.btAjouterAct.UseVisualStyleBackColor = true;
            this.btAjouterAct.Click += new System.EventHandler(this.btAjouterAct_Click);
            // 
            // descrAct
            // 
            this.descrAct.AutoSize = true;
            this.descrAct.Location = new System.Drawing.Point(427, 180);
            this.descrAct.Name = "descrAct";
            this.descrAct.Size = new System.Drawing.Size(66, 13);
            this.descrAct.TabIndex = 33;
            this.descrAct.Text = "Description :";
            // 
            // libAct
            // 
            this.libAct.AutoSize = true;
            this.libAct.Location = new System.Drawing.Point(451, 83);
            this.libAct.Name = "libAct";
            this.libAct.Size = new System.Drawing.Size(29, 13);
            this.libAct.TabIndex = 32;
            this.libAct.Text = "Nom";
            // 
            // btSupprimerAct
            // 
            this.btSupprimerAct.Location = new System.Drawing.Point(367, 361);
            this.btSupprimerAct.Name = "btSupprimerAct";
            this.btSupprimerAct.Size = new System.Drawing.Size(87, 41);
            this.btSupprimerAct.TabIndex = 29;
            this.btSupprimerAct.Text = "Supprimer";
            this.btSupprimerAct.UseVisualStyleBackColor = true;
            this.btSupprimerAct.Click += new System.EventHandler(this.btSupprimerAct_Click);
            // 
            // btModifierAct
            // 
            this.btModifierAct.Location = new System.Drawing.Point(664, 348);
            this.btModifierAct.Name = "btModifierAct";
            this.btModifierAct.Size = new System.Drawing.Size(93, 54);
            this.btModifierAct.TabIndex = 28;
            this.btModifierAct.Text = "Modifier";
            this.btModifierAct.UseVisualStyleBackColor = true;
            this.btModifierAct.Click += new System.EventHandler(this.btModifierAct_Click);
            // 
            // listeBoxAct
            // 
            this.listeBoxAct.FormattingEnabled = true;
            this.listeBoxAct.Location = new System.Drawing.Point(52, 74);
            this.listeBoxAct.Name = "listeBoxAct";
            this.listeBoxAct.Size = new System.Drawing.Size(239, 238);
            this.listeBoxAct.TabIndex = 26;
            this.listeBoxAct.SelectedIndexChanged += new System.EventHandler(this.listeBoxAct_SelectedIndexChanged);
            // 
            // bt_Equipement
            // 
            this.bt_Equipement.Location = new System.Drawing.Point(25, 21);
            this.bt_Equipement.Name = "bt_Equipement";
            this.bt_Equipement.Size = new System.Drawing.Size(88, 23);
            this.bt_Equipement.TabIndex = 39;
            this.bt_Equipement.Text = "Equipements";
            this.bt_Equipement.UseVisualStyleBackColor = true;
            this.bt_Equipement.Click += new System.EventHandler(this.bt_Equipement_Click);
            // 
            // lbDuree
            // 
            this.lbDuree.AutoSize = true;
            this.lbDuree.Location = new System.Drawing.Point(427, 118);
            this.lbDuree.Name = "lbDuree";
            this.lbDuree.Size = new System.Drawing.Size(77, 13);
            this.lbDuree.TabIndex = 40;
            this.lbDuree.Text = "Durée (heures)";
            // 
            // btUser
            // 
            this.btUser.Location = new System.Drawing.Point(171, 21);
            this.btUser.Name = "btUser";
            this.btUser.Size = new System.Drawing.Size(88, 23);
            this.btUser.TabIndex = 44;
            this.btUser.Text = "Clients";
            this.btUser.UseVisualStyleBackColor = true;
            this.btUser.Click += new System.EventHandler(this.btUser_Click);
            // 
            // libClientMax
            // 
            this.libClientMax.AutoSize = true;
            this.libClientMax.Location = new System.Drawing.Point(427, 154);
            this.libClientMax.Name = "libClientMax";
            this.libClientMax.Size = new System.Drawing.Size(101, 13);
            this.libClientMax.TabIndex = 45;
            this.libClientMax.Text = "Nb. max. participant";
            // 
            // nbMaxPart
            // 
            this.nbMaxPart.Location = new System.Drawing.Point(531, 147);
            this.nbMaxPart.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nbMaxPart.Name = "nbMaxPart";
            this.nbMaxPart.Size = new System.Drawing.Size(120, 20);
            this.nbMaxPart.TabIndex = 47;
            // 
            // btVider
            // 
            this.btVider.Location = new System.Drawing.Point(722, 80);
            this.btVider.Name = "btVider";
            this.btVider.Size = new System.Drawing.Size(87, 41);
            this.btVider.TabIndex = 48;
            this.btVider.Text = "Vider les champs";
            this.btVider.UseVisualStyleBackColor = true;
            this.btVider.Click += new System.EventHandler(this.btVider_Click);
            // 
            // btPlanning
            // 
            this.btPlanning.Location = new System.Drawing.Point(298, 21);
            this.btPlanning.Name = "btPlanning";
            this.btPlanning.Size = new System.Drawing.Size(88, 23);
            this.btPlanning.TabIndex = 49;
            this.btPlanning.Text = "Planning";
            this.btPlanning.UseVisualStyleBackColor = true;
            this.btPlanning.Click += new System.EventHandler(this.btPlanning_Click);
            // 
            // cbDuree
            // 
            this.cbDuree.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDuree.FormattingEnabled = true;
            this.cbDuree.Location = new System.Drawing.Point(531, 115);
            this.cbDuree.Name = "cbDuree";
            this.cbDuree.Size = new System.Drawing.Size(121, 21);
            this.cbDuree.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(427, 299);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Equipement";
            // 
            // cbEq
            // 
            this.cbEq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEq.FormattingEnabled = true;
            this.cbEq.Location = new System.Drawing.Point(535, 299);
            this.cbEq.Name = "cbEq";
            this.cbEq.Size = new System.Drawing.Size(134, 21);
            this.cbEq.TabIndex = 43;
            // 
            // v_Activite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 463);
            this.Controls.Add(this.cbDuree);
            this.Controls.Add(this.btPlanning);
            this.Controls.Add(this.btVider);
            this.Controls.Add(this.nbMaxPart);
            this.Controls.Add(this.libClientMax);
            this.Controls.Add(this.btUser);
            this.Controls.Add(this.cbEq);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbDuree);
            this.Controls.Add(this.bt_Equipement);
            this.Controls.Add(this.txtDescrAct);
            this.Controls.Add(this.txtLibAct);
            this.Controls.Add(this.btAjouterAct);
            this.Controls.Add(this.descrAct);
            this.Controls.Add(this.libAct);
            this.Controls.Add(this.btSupprimerAct);
            this.Controls.Add(this.btModifierAct);
            this.Controls.Add(this.listeBoxAct);
            this.Name = "v_Activite";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Activités";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.v_Activite_FormClosing);
            this.Load += new System.EventHandler(this.v_Activite_Load);
            this.VisibleChanged += new System.EventHandler(this.v_Activite_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.nbMaxPart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDescrAct;
        private System.Windows.Forms.TextBox txtLibAct;
        private System.Windows.Forms.Button btAjouterAct;
        private System.Windows.Forms.Label descrAct;
        private System.Windows.Forms.Label libAct;
        private System.Windows.Forms.Button btSupprimerAct;
        private System.Windows.Forms.Button btModifierAct;
        private System.Windows.Forms.ListBox listeBoxAct;
        private System.Windows.Forms.Button bt_Equipement;
        private System.Windows.Forms.Label lbDuree;
        private System.Windows.Forms.Button btUser;
        private System.Windows.Forms.Label libClientMax;
        private System.Windows.Forms.NumericUpDown nbMaxPart;
        private System.Windows.Forms.Button btVider;
        private System.Windows.Forms.Button btPlanning;
        private System.Windows.Forms.ComboBox cbDuree;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbEq;
    }
}

