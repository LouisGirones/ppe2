﻿namespace PPE2
{
    partial class v_Client
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLibNom = new System.Windows.Forms.TextBox();
            this.txtLibPrenom = new System.Windows.Forms.TextBox();
            this.btAjouterClient = new System.Windows.Forms.Button();
            this.libNom = new System.Windows.Forms.Label();
            this.libPrenom = new System.Windows.Forms.Label();
            this.btSupprimerClient = new System.Windows.Forms.Button();
            this.listeBoxCli = new System.Windows.Forms.ListBox();
            this.btActivite = new System.Windows.Forms.Button();
            this.btEquipement = new System.Windows.Forms.Button();
            this.rbHomme = new System.Windows.Forms.RadioButton();
            this.rbFemme = new System.Windows.Forms.RadioButton();
            this.lbSexeCli = new System.Windows.Forms.Label();
            this.btVider = new System.Windows.Forms.Button();
            this.btPlanning = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtLibNom
            // 
            this.txtLibNom.Location = new System.Drawing.Point(708, 153);
            this.txtLibNom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLibNom.MaxLength = 32;
            this.txtLibNom.Name = "txtLibNom";
            this.txtLibNom.Size = new System.Drawing.Size(160, 22);
            this.txtLibNom.TabIndex = 38;
            // 
            // txtLibPrenom
            // 
            this.txtLibPrenom.Location = new System.Drawing.Point(708, 98);
            this.txtLibPrenom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLibPrenom.MaxLength = 32;
            this.txtLibPrenom.Name = "txtLibPrenom";
            this.txtLibPrenom.Size = new System.Drawing.Size(160, 22);
            this.txtLibPrenom.TabIndex = 36;
            // 
            // btAjouterClient
            // 
            this.btAjouterClient.Location = new System.Drawing.Point(713, 434);
            this.btAjouterClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btAjouterClient.Name = "btAjouterClient";
            this.btAjouterClient.Size = new System.Drawing.Size(109, 60);
            this.btAjouterClient.TabIndex = 35;
            this.btAjouterClient.Text = "Ajouter";
            this.btAjouterClient.UseVisualStyleBackColor = true;
            this.btAjouterClient.Click += new System.EventHandler(this.btAjouterClient_Click);
            // 
            // libNom
            // 
            this.libNom.AutoSize = true;
            this.libNom.Location = new System.Drawing.Point(601, 156);
            this.libNom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.libNom.Name = "libNom";
            this.libNom.Size = new System.Drawing.Size(37, 17);
            this.libNom.TabIndex = 34;
            this.libNom.Text = "Nom";
            // 
            // libPrenom
            // 
            this.libPrenom.AutoSize = true;
            this.libPrenom.Location = new System.Drawing.Point(601, 102);
            this.libPrenom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.libPrenom.Name = "libPrenom";
            this.libPrenom.Size = new System.Drawing.Size(57, 17);
            this.libPrenom.TabIndex = 32;
            this.libPrenom.Text = "Prenom";
            // 
            // btSupprimerClient
            // 
            this.btSupprimerClient.Location = new System.Drawing.Point(489, 444);
            this.btSupprimerClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btSupprimerClient.Name = "btSupprimerClient";
            this.btSupprimerClient.Size = new System.Drawing.Size(116, 50);
            this.btSupprimerClient.TabIndex = 29;
            this.btSupprimerClient.Text = "Supprimer";
            this.btSupprimerClient.UseVisualStyleBackColor = true;
            this.btSupprimerClient.Click += new System.EventHandler(this.btSupprimerClient_Click);
            // 
            // listeBoxCli
            // 
            this.listeBoxCli.FormattingEnabled = true;
            this.listeBoxCli.ItemHeight = 16;
            this.listeBoxCli.Location = new System.Drawing.Point(69, 91);
            this.listeBoxCli.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listeBoxCli.Name = "listeBoxCli";
            this.listeBoxCli.Size = new System.Drawing.Size(317, 292);
            this.listeBoxCli.TabIndex = 26;
            this.listeBoxCli.SelectedIndexChanged += new System.EventHandler(this.listeBoxCli_SelectedIndexChanged);
            // 
            // btActivite
            // 
            this.btActivite.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btActivite.Location = new System.Drawing.Point(39, 16);
            this.btActivite.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btActivite.Name = "btActivite";
            this.btActivite.Size = new System.Drawing.Size(100, 28);
            this.btActivite.TabIndex = 39;
            this.btActivite.Text = "Activités";
            this.btActivite.UseVisualStyleBackColor = true;
            this.btActivite.Click += new System.EventHandler(this.btActivite_Click);
            // 
            // btEquipement
            // 
            this.btEquipement.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btEquipement.Location = new System.Drawing.Point(193, 16);
            this.btEquipement.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btEquipement.Name = "btEquipement";
            this.btEquipement.Size = new System.Drawing.Size(100, 28);
            this.btEquipement.TabIndex = 40;
            this.btEquipement.Text = "Equipements";
            this.btEquipement.UseVisualStyleBackColor = true;
            this.btEquipement.Click += new System.EventHandler(this.btEquipement_Click);
            // 
            // rbHomme
            // 
            this.rbHomme.AutoSize = true;
            this.rbHomme.Location = new System.Drawing.Point(708, 222);
            this.rbHomme.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbHomme.Name = "rbHomme";
            this.rbHomme.Size = new System.Drawing.Size(44, 21);
            this.rbHomme.TabIndex = 41;
            this.rbHomme.TabStop = true;
            this.rbHomme.Tag = "Homme";
            this.rbHomme.Text = "M.";
            this.rbHomme.UseVisualStyleBackColor = true;
            // 
            // rbFemme
            // 
            this.rbFemme.AutoSize = true;
            this.rbFemme.Location = new System.Drawing.Point(784, 222);
            this.rbFemme.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbFemme.Name = "rbFemme";
            this.rbFemme.Size = new System.Drawing.Size(59, 21);
            this.rbFemme.TabIndex = 42;
            this.rbFemme.TabStop = true;
            this.rbFemme.Tag = "Femme";
            this.rbFemme.Text = "Mme";
            this.rbFemme.UseVisualStyleBackColor = true;
            // 
            // lbSexeCli
            // 
            this.lbSexeCli.AutoSize = true;
            this.lbSexeCli.Location = new System.Drawing.Point(601, 224);
            this.lbSexeCli.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbSexeCli.Name = "lbSexeCli";
            this.lbSexeCli.Size = new System.Drawing.Size(39, 17);
            this.lbSexeCli.TabIndex = 43;
            this.lbSexeCli.Text = "Sexe";
            // 
            // btVider
            // 
            this.btVider.Location = new System.Drawing.Point(944, 98);
            this.btVider.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btVider.Name = "btVider";
            this.btVider.Size = new System.Drawing.Size(116, 50);
            this.btVider.TabIndex = 49;
            this.btVider.Text = "Vider les champs";
            this.btVider.UseVisualStyleBackColor = true;
            this.btVider.Click += new System.EventHandler(this.btVider_Click);
            // 
            // btPlanning
            // 
            this.btPlanning.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btPlanning.Location = new System.Drawing.Point(356, 16);
            this.btPlanning.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPlanning.Name = "btPlanning";
            this.btPlanning.Size = new System.Drawing.Size(100, 28);
            this.btPlanning.TabIndex = 51;
            this.btPlanning.Text = "Planning";
            this.btPlanning.UseVisualStyleBackColor = true;
            this.btPlanning.Click += new System.EventHandler(this.btPlanning_Click);
            // 
            // v_Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1140, 570);
            this.Controls.Add(this.btPlanning);
            this.Controls.Add(this.btVider);
            this.Controls.Add(this.lbSexeCli);
            this.Controls.Add(this.rbFemme);
            this.Controls.Add(this.rbHomme);
            this.Controls.Add(this.btEquipement);
            this.Controls.Add(this.btActivite);
            this.Controls.Add(this.txtLibNom);
            this.Controls.Add(this.txtLibPrenom);
            this.Controls.Add(this.btAjouterClient);
            this.Controls.Add(this.libNom);
            this.Controls.Add(this.libPrenom);
            this.Controls.Add(this.btSupprimerClient);
            this.Controls.Add(this.listeBoxCli);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "v_Client";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Clients";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.v_Client_FormClosing);
            this.Load += new System.EventHandler(this.v_Client_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLibNom;
        private System.Windows.Forms.TextBox txtLibPrenom;
        private System.Windows.Forms.Button btAjouterClient;
        private System.Windows.Forms.Label libNom;
        private System.Windows.Forms.Label libPrenom;
        private System.Windows.Forms.Button btSupprimerClient;
        private System.Windows.Forms.ListBox listeBoxCli;
        private System.Windows.Forms.Button btActivite;
        private System.Windows.Forms.Button btEquipement;
        private System.Windows.Forms.RadioButton rbHomme;
        private System.Windows.Forms.RadioButton rbFemme;
        private System.Windows.Forms.Label lbSexeCli;
        private System.Windows.Forms.Button btVider;
        private System.Windows.Forms.Button btPlanning;
    }
}

