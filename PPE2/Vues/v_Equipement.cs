﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PPE2
{
    public partial class v_Equipement : Form
    {
        public v_Equipement()
        {
            InitializeComponent();
        }

        private void v_Equipement_Load(object sender, EventArgs e)
        {
            listeBoxEq.DataSource = null;
            listeBoxEq.DataSource = EquipementDAO.GetEquipements();
        }

        private void btAjouterEq_Click(object sender, EventArgs e)
        {
            if (!Verification.IsEmpty(txtLibEq.Text) && !Verification.IsEmpty(txtDescrEq.Text) && Verification.SuperieurAZero((int)nbEq.Value))
            {
                if (!EquipementDAO.ExisteEquipement(txtLibEq.Text))
                {
                    Equipement eq;
                    eq = new Equipement(txtLibEq.Text, txtDescrEq.Text, (int)nbEq.Value);
                    EquipementDAO.InsererEquipement(eq);
                    listeBoxEq.DataSource = null;
                    listeBoxEq.DataSource = EquipementDAO.GetEquipements();
                }
                else
                {
                    MessageBox.Show("Cet équipement existe déjà.");
                }
            }
            else
            {
                MessageBox.Show("Vous devez entrer un libelle, une description et un nombre.");
            }

        }

        private void btSupprimerEq_Click(object sender, EventArgs e)
        {
            if (listeBoxEq.SelectedItem != null)
            {
                if (Outils.ConfirmationModal("supprimer cet équipement?"))
                {
                    Equipement eq = (Equipement)listeBoxEq.SelectedItem;
                    EquipementDAO.SupprimerEquipement(eq);
                    listeBoxEq.DataSource = null;
                    listeBoxEq.DataSource = EquipementDAO.GetEquipements();
                }
            }
            else
            {
                MessageBox.Show("Vous devez selectionner un équipement.");
            }
        }

        private void listeBoxEq_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listeBoxEq.SelectedItem != null)
            {
                Equipement eq = (Equipement)listeBoxEq.SelectedItem;
                txtLibEq.Text = eq.Libelle;
                txtDescrEq.Text = eq.Description;
                nbEq.Value = (int)eq.Nombre;
            }
        }

        private void btModifierEq_Click(object sender, EventArgs e)
        {
            if (listeBoxEq.SelectedItem != null)
            {
                if (!Verification.IsEmpty(txtLibEq.Text) && !Verification.IsEmpty(txtDescrEq.Text) && Verification.SuperieurAZero((int)nbEq.Value))
                {
                        Equipement eq;
                        eq = (Equipement)listeBoxEq.SelectedItem;
                        eq.Libelle = txtLibEq.Text;
                        eq.Description = txtDescrEq.Text;
                        eq.Nombre = (int)nbEq.Value;
                        EquipementDAO.ModifierEquipement(eq);
                        listeBoxEq.DataSource = null;
                        listeBoxEq.DataSource = EquipementDAO.GetEquipements();
                }
                else
                {
                    MessageBox.Show("Vous devez entrer un libellé, une description et un nombre."); 
                }
            }
            else
            {
                MessageBox.Show("Vous devez d'abord choisir un équipement.");
            }
        }

        private void btVider_Click(object sender, EventArgs e)
        {
            Outils.ViderTextBoxs(this);
            listeBoxEq.SelectedItem = null;
            nbEq.Value = (int)0;
        }

        #region SwitchForms
        private void btActivite_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Act.Show();
        }

        private void btClients_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Cli.Show();
        }

        private void btPlanning_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Plan.Show();
        }
        #endregion

        private void v_Equipement_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
