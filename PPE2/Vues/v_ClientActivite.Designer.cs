﻿namespace PPE2
{
    partial class v_ClientActivite
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.listeBoxCli = new System.Windows.Forms.ListBox();
            this.btRetour = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxPart = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btPlacerClient = new System.Windows.Forms.Button();
            this.btRetirerClient = new System.Windows.Forms.Button();
            this.btRetirerClients = new System.Windows.Forms.Button();
            this.lbInfos = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listeBoxCli
            // 
            this.listeBoxCli.FormattingEnabled = true;
            this.listeBoxCli.ItemHeight = 16;
            this.listeBoxCli.Location = new System.Drawing.Point(76, 144);
            this.listeBoxCli.Margin = new System.Windows.Forms.Padding(4);
            this.listeBoxCli.Name = "listeBoxCli";
            this.listeBoxCli.Size = new System.Drawing.Size(317, 292);
            this.listeBoxCli.TabIndex = 26;
            // 
            // btRetour
            // 
            this.btRetour.Location = new System.Drawing.Point(152, 457);
            this.btRetour.Margin = new System.Windows.Forms.Padding(4);
            this.btRetour.Name = "btRetour";
            this.btRetour.Size = new System.Drawing.Size(116, 50);
            this.btRetour.TabIndex = 50;
            this.btRetour.Text = "Retour";
            this.btRetour.UseVisualStyleBackColor = true;
            this.btRetour.Click += new System.EventHandler(this.btRetour_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 17);
            this.label1.TabIndex = 51;
            this.label1.Text = "Liste des clients";
            // 
            // listBoxPart
            // 
            this.listBoxPart.FormattingEnabled = true;
            this.listBoxPart.ItemHeight = 16;
            this.listBoxPart.Location = new System.Drawing.Point(735, 144);
            this.listBoxPart.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxPart.Name = "listBoxPart";
            this.listBoxPart.Size = new System.Drawing.Size(317, 292);
            this.listBoxPart.TabIndex = 52;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(732, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 53;
            this.label2.Text = "Participants";
            // 
            // btPlacerClient
            // 
            this.btPlacerClient.Location = new System.Drawing.Point(468, 197);
            this.btPlacerClient.Margin = new System.Windows.Forms.Padding(4);
            this.btPlacerClient.Name = "btPlacerClient";
            this.btPlacerClient.Size = new System.Drawing.Size(116, 50);
            this.btPlacerClient.TabIndex = 54;
            this.btPlacerClient.Text = "Ajouter";
            this.btPlacerClient.UseVisualStyleBackColor = true;
            this.btPlacerClient.Click += new System.EventHandler(this.btPlacerClient_Click);
            // 
            // btRetirerClient
            // 
            this.btRetirerClient.Location = new System.Drawing.Point(468, 264);
            this.btRetirerClient.Margin = new System.Windows.Forms.Padding(4);
            this.btRetirerClient.Name = "btRetirerClient";
            this.btRetirerClient.Size = new System.Drawing.Size(116, 50);
            this.btRetirerClient.TabIndex = 55;
            this.btRetirerClient.Text = "Retirer";
            this.btRetirerClient.UseVisualStyleBackColor = true;
            this.btRetirerClient.Click += new System.EventHandler(this.btRetirerClient_Click);
            // 
            // btRetirerClients
            // 
            this.btRetirerClients.Location = new System.Drawing.Point(468, 337);
            this.btRetirerClients.Margin = new System.Windows.Forms.Padding(4);
            this.btRetirerClients.Name = "btRetirerClients";
            this.btRetirerClients.Size = new System.Drawing.Size(116, 50);
            this.btRetirerClients.TabIndex = 56;
            this.btRetirerClients.Text = "Retirer tous";
            this.btRetirerClients.UseVisualStyleBackColor = true;
            this.btRetirerClients.Click += new System.EventHandler(this.btRetirerClients_Click);
            // 
            // lbInfos
            // 
            this.lbInfos.AutoSize = true;
            this.lbInfos.Location = new System.Drawing.Point(363, 27);
            this.lbInfos.Name = "lbInfos";
            this.lbInfos.Size = new System.Drawing.Size(46, 17);
            this.lbInfos.TabIndex = 57;
            this.lbInfos.Text = "label3";
            // 
            // v_ClientActivite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1140, 570);
            this.Controls.Add(this.lbInfos);
            this.Controls.Add(this.btRetirerClients);
            this.Controls.Add(this.btRetirerClient);
            this.Controls.Add(this.btPlacerClient);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBoxPart);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btRetour);
            this.Controls.Add(this.listeBoxCli);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "v_ClientActivite";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Clients-Activités";
            this.Load += new System.EventHandler(this.v_Equipement_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listeBoxCli;
        private System.Windows.Forms.Button btRetour;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxPart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btPlacerClient;
        private System.Windows.Forms.Button btRetirerClient;
        private System.Windows.Forms.Button btRetirerClients;
        private System.Windows.Forms.Label lbInfos;
    }
}

