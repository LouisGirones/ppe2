﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PPE2
{
    public partial class v_ClientActivite : Form
    {
        private Planning p;

        public v_ClientActivite(Planning pl)
        {
            InitializeComponent();
            this.p = pl;
            
        }

        private void v_Equipement_Load(object sender, EventArgs e)
        {
            listeBoxCli.DataSource = null;
            listBoxPart.DataSource = null;
            listeBoxCli.DataSource = ReserveDAO.GetClients(p);
            listBoxPart.DataSource = ReserveDAO.GetParticipants(p);
            DateTime dureeAct = Convert.ToDateTime(p.Act.Duree);
            lbInfos.Text = "Activité " + p.Act.Nom + " le " + p.DtJourHeureAct.ToString("yyyy-MM-dd") +
                " de " + p.DtJourHeureAct.ToString("HH:mm:ss") + " à " + 
                p.DtJourHeureAct.Add(new TimeSpan(dureeAct.Hour, dureeAct.Minute, 0)).ToString("HH:mm:ss")+
                " (max : "+p.Act.NbClient+")";
            
        }

        private void btPlacerClient_Click(object sender, EventArgs e)
        {
            if (listeBoxCli.SelectedItem != null)
            {
                if (listBoxPart.Items.Count < p.Act.NbClient)
                {
                    Client c = (Client)listeBoxCli.SelectedItem;
                    if (!ReserveDAO.ExisteClientAct(p, c))
                    {
                        ReserveDAO.InsererResa(c, p);
                        listeBoxCli.DataSource = null;
                        listBoxPart.DataSource = null;
                        listeBoxCli.DataSource = ReserveDAO.GetClients(p);
                        listBoxPart.DataSource = ReserveDAO.GetParticipants(p);
                    }
                    else
                    {
                        MessageBox.Show("Ce client est déja placé sur cette activité");
                    }
                }
                else
                {
                    MessageBox.Show("Le nombre de participant maximal est déja atteint");
                }
            }
            else
            {
                MessageBox.Show("Vous devez sélectionner un client");
            }
        }

        private void btRetirerClient_Click(object sender, EventArgs e)
        {
            if (listBoxPart.SelectedItem != null)
            {
                Client c = (Client)listBoxPart.SelectedItem;
                ReserveDAO.SupprimerClient(c, p);
                listeBoxCli.DataSource = null;
                listBoxPart.DataSource = null;
                listeBoxCli.DataSource = ReserveDAO.GetClients(p);
                listBoxPart.DataSource = ReserveDAO.GetParticipants(p);
            }
            else
            {
                MessageBox.Show("Vous devez sélectionner un client dans la liste des participants");
            }
        }

        private void btRetirerClients_Click(object sender, EventArgs e)
        {
            if (Outils.ConfirmationModal("supprimer l'ensemble des clients pour cette activité ?") && listBoxPart.Items.Count>0)
            {
                ReserveDAO.SupprimerClients(p);
                listeBoxCli.DataSource = null;
                listBoxPart.DataSource = null;
                listeBoxCli.DataSource = ReserveDAO.GetClients(p);
                listBoxPart.DataSource = ReserveDAO.GetParticipants(p);
            }
        }

        private void btRetour_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }



    }
}
