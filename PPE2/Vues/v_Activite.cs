﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace PPE2
{
    public partial class v_Activite : Form
    {
        public v_Activite()
        {  
            InitializeComponent();
        }

        private void v_Activite_Load(object sender, EventArgs e)
        {
            SetComboboxDuree();
            cbDuree.SelectedIndex = 0; 
            //Données liées à l'activité
            if (listeBoxAct.SelectedItem != null)
            {
                Activite a = (Activite)listeBoxAct.SelectedItem;
                SetComboboxEqSelectedItem(a.IdEq);
                SetComboboxDureeSelectedItem(a.Duree);
            }
            
        }

        private void btAjouterAct_Click(object sender, EventArgs e)
        {
            //Contrôles de validités
            if (!Verification.IsEmpty(txtLibAct.Text) && !Verification.IsEmpty(txtDescrAct.Text) && 
                Verification.SuperieurAZero((int)nbMaxPart.Value))
            {
                //Vérif si existe dans la db
                if (!ActiviteDAO.ExisteActivite(txtLibAct.Text))
                {
                    Equipement eq = (Equipement)cbEq.SelectedItem;
                    //Verif que le nombre de participant n'est pas > au nombre d'équipement
                    if (Verification.VerifNbPartNbEq((int)nbMaxPart.Value, eq.Nombre) || eq.Libelle == "Aucun")
                    {
                        //Enregistrement de l'activité
                        Activite a = new Activite(eq.Id, txtLibAct.Text, txtDescrAct.Text, cbDuree.SelectedItem.ToString(), (int)nbMaxPart.Value);
                        ActiviteDAO.InsererActivite(a, eq);
                        listeBoxAct.DataSource = null;
                        listeBoxAct.DataSource = ActiviteDAO.GetActivites();
                    }
                    else 
                    {
                        MessageBox.Show("Maximum "+eq.Nombre+" participant pour cet équipement");
                    }
                }
                else
                {
                    MessageBox.Show("Cette activité existe déjà.");
                }
            }
            else
            {
                MessageBox.Show("Vous devez remplir tous les champs.");
            }   
        }

        private void btSupprimerAct_Click(object sender, EventArgs e)
        {
            if (listeBoxAct.SelectedItem != null)
            {
                if (Outils.ConfirmationModal("supprimer cette activité?"))
                {
                    Activite a = (Activite)listeBoxAct.SelectedItem;
                    ActiviteDAO.SupprimerActivite(a);
                    listeBoxAct.DataSource = null;
                    listeBoxAct.DataSource = ActiviteDAO.GetActivites();
                }
            }
            else
            {
                MessageBox.Show("Vous devez selectionner une activité.");
            }
        }

        private void listeBoxAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listeBoxAct.SelectedItem != null)
            {
                Activite a = (Activite)listeBoxAct.SelectedItem;
                //Remplissage des champs du formulaire avec les valeurs de l'objet act
                txtLibAct.Text = a.Nom;
                txtDescrAct.Text = a.Description;
                cbDuree.SelectedValue = a.Duree;
                nbMaxPart.Value = (int)a.NbClient;
                SetComboboxEqSelectedItem(a.IdEq);
                SetComboboxDureeSelectedItem(a.Duree);
            }
        }

        private void btModifierAct_Click(object sender, EventArgs e)
        {
            if (listeBoxAct.SelectedItem != null)
            {
                if (!Verification.IsEmpty(txtLibAct.Text) && !Verification.IsEmpty(txtDescrAct.Text) 
                    && Verification.SuperieurAZero((int)nbMaxPart.Value))
                {
                    Activite a = (Activite)listeBoxAct.SelectedItem;
                    Equipement eq = (Equipement)cbEq.SelectedItem;
                    //Le nb de participant est inferieur au nombre d'equipement ou l'equipement est aucun
                    if (Verification.VerifNbPartNbEq((int)nbMaxPart.Value, eq.Nombre) || eq.Libelle == "Aucun")
                    {
                        a.Nom = txtLibAct.Text;
                        a.Description = txtDescrAct.Text;
                        a.Duree = cbDuree.SelectedItem.ToString();
                        a.NbClient = (int)nbMaxPart.Value;
                        a.IdEq = eq.Id;
                        ActiviteDAO.ModifierActivite(a); //Update de l'activité
                        MessageBox.Show("L'activité a bien été modifiée");
                        //Actualisation de la liste
                        listeBoxAct.DataSource = null;
                        listeBoxAct.DataSource = ActiviteDAO.GetActivites();
                    }
                    else
                    {
                        MessageBox.Show("Maximum " + eq.Nombre + " participant pour cet équipement");
                    }
                }
                else
                {
                    MessageBox.Show("Vous devez remplir tous les champs.");
                }
            }
            else
            {
                MessageBox.Show("Vous devez d'abord choisir une activité.");
            }
        }

        private void btVider_Click(object sender, EventArgs e)
        {
            Outils.ViderTextBoxs(this);
            listeBoxAct.SelectedItem = null;
            nbMaxPart.Value = (int)0;
            cbDuree.SelectedIndex = 0;
            cbEq.SelectedValue = "Aucun";
        }

        private void SetComboboxEqSelectedItem(int idAct)
        {
            foreach (Equipement eq in cbEq.Items)
            {
                if (eq.Id == idAct)
                {
                    cbEq.SelectedItem = eq;
                }
            }
        }

        private void SetComboboxDureeSelectedItem(string duree)
        {
            foreach (string s in cbDuree.Items)
            {
                if (s == duree)
                {
                    cbDuree.SelectedItem = s;
                }
            }
        }

        /// <summary>
        /// Définies les valeurs possibles de la combobox
        /// </summary>
        private void SetComboboxDuree()
        {
            DateTime timeloop = new DateTime(0);
            timeloop = timeloop.Add(new TimeSpan(0, 30, 0));
            for (int i = 0; i < 10; i++)
            {
                cbDuree.Items.Add(timeloop.ToString("HH:mm"));
                //Colonne des horaire
                timeloop = timeloop.Add(new TimeSpan(0, 30, 0));
            } 
        }

        #region SwitchForms
        private void bt_Equipement_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Eq.Show();
        }

        private void btUser_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Cli.Show();
        }

        private void btPlanning_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.v_Plan.Show();
        }
        #endregion

        private void v_Activite_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void v_Activite_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                //listbox activites
                listeBoxAct.DataSource = null;
                cbEq.DataSource = null;
                cbEq.ValueMember = null;
                cbEq.DataSource = EquipementDAO.GetEquipements();
                listeBoxAct.DataSource = ActiviteDAO.GetActivites();
                cbEq.ValueMember = "libelle";
            }
        }
    }
}
