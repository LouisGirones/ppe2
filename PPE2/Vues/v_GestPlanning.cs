﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PPE2
{
    public partial class v_GestPlanning : Form
    {
        private DateTime week;

        public v_GestPlanning(DateTime week)
        {
            InitializeComponent();
            this.week = week;
        }

        private void v_GestPlanning_Load(object sender, EventArgs e)
        {
            lbWeek.Text = "Semaine du " + week.ToString("D") + " au " + week.Add(new TimeSpan(4, 0, 0, 0)).ToString("D");
            //listbox planning
            listeBoxPlanning.DataSource = null;
            listeBoxPlanning.DataSource = PlanningDAO.GetPlanning(week.Date.ToString("yyyy-MM-dd"), week.Add(new TimeSpan(5, 23, 59, 59)).Date.ToString("yyyy-MM-dd"));
            cbAct.DataSource = null;
            cbAct.ValueMember = null;
            cbAct.DataSource = ActiviteDAO.GetActivites();
            cbAct.ValueMember = "nom";
            SetCbJours();
            cbJours.SelectedIndex = 0;
            SetCbHeures();
            cbHeure.SelectedIndex = 0;
            if (listeBoxPlanning.SelectedItem != null)
            {
                Planning p = (Planning)listeBoxPlanning.SelectedItem;
                SetComboboxActSelectedItem(p.Act.Id);
                SetComboboxJoursSelectedItem(p.DtJourHeureAct.ToString("D"));
                SetComboboxHeureSelectedItem(p.DtJourHeureAct.ToString("HH:mm"));
            }

        }

        private void btAjouterPlanning_Click(object sender, EventArgs e)
        {
            //Construction de l'objet DateTime à partir du jour et de l'heure
            DateTime jour = Convert.ToDateTime(cbJours.SelectedItem);
            DateTime heure = Convert.ToDateTime(cbHeure.SelectedItem);
            DateTime dt = new DateTime(jour.Year, jour.Month, jour.Day, heure.Hour, heure.Minute, 0);
            //Verif qu'une act est selecrt
            if (cbAct.SelectedItem != null)
            {
                //Verif qu'une activité ne commence pas à cette heure ci
                if (!PlanningDAO.ExistePlanning(dt.ToString("yyyy-MM-dd HH:mm:ss")))
                {
                    Activite a = (Activite)cbAct.SelectedItem;
                    DateTime dureeAct = Convert.ToDateTime(a.Duree);
                    DateTime dtJourHeure = new DateTime(jour.Year, jour.Month, jour.Day, 17, 0, 0);
                    //Soustraction d'une seconde pour que la date ne match pas en cas d'égalité 
                    //e.g : act de 10h a 11h et une act commencant a 11h
                    dureeAct = dureeAct.AddSeconds(-1);
                    //Verif qu'une activité ne sort pas du planning (heure max = 17h)
                    if (dt.Add(dureeAct.TimeOfDay) < dtJourHeure)
                    {
                        //Verif qu'une activité n'a pas lieu pendant la durée entrée
                        if (!PlanningDAO.EmpieteActiviteDebut(dt.ToString("yyyy-MM-dd HH:mm:ss"), dt.Add(dureeAct.TimeOfDay).ToString("yyyy-MM-dd HH:mm:ss")))
                        {
                            //Verif qu'une activité n'est pas déjà en cours 
                            if (!PlanningDAO.EmpieteSurActivite(dt.ToString("yyyy-MM-dd HH:mm:ss")))
                            {
                                Planning p = new Planning(dt, a);
                                PlanningDAO.InsererActivitePlanning(p);
                                listeBoxPlanning.DataSource = null;
                                listeBoxPlanning.DataSource = PlanningDAO.GetPlanning(week.ToString("yyyy-MM-dd"), week.Add(new TimeSpan(5, 23, 59, 59)).ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                MessageBox.Show("Une activité est déjà en cours");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Cette activité empiète sur une activité se déroulant plus tard");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Une activité ne peut se terminer après 17h.");
                    }
                }
                else
                {
                    MessageBox.Show("Une activité commence déjà à cette heure");
                }
            }
            else
            {
                MessageBox.Show("Vous devez sélectionné une activité");
            }
        }

        private void btSupprimerPlanning_Click(object sender, EventArgs e)
        {
            if (listeBoxPlanning.SelectedItem != null)
            {
                if (Outils.ConfirmationModal("supprimer cette cet activité du planning?"))
                {
                    Planning p = (Planning)listeBoxPlanning.SelectedItem;
                    PlanningDAO.SupprimerPlanning(p);
                    listeBoxPlanning.DataSource = null;
                    listeBoxPlanning.DataSource = PlanningDAO.GetPlanning(week.ToString("yyyy-MM-dd"), week.Add(new TimeSpan(4, 23, 59, 59)).ToString("yyyy-MM-dd"));
                }
            }
            else
            {
                MessageBox.Show("Vous devez selectionner une activité.");
            }
        }

        private void listeBoxPlanning_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listeBoxPlanning.SelectedItem != null)
            {
                Planning p = (Planning)listeBoxPlanning.SelectedItem;
                SetComboboxActSelectedItem(p.Act.Id);
                SetComboboxJoursSelectedItem(p.DtJourHeureAct.ToString("D"));
                SetComboboxHeureSelectedItem(p.DtJourHeureAct.ToString("HH:mm"));
            }
        }

        private void btModifierPlanning_Click(object sender, EventArgs e)
        {
            if (listeBoxPlanning.SelectedItem != null)
            {
                //Construction de l'objet DateTime à partir du jour et de l'heure
                DateTime jour = Convert.ToDateTime(cbJours.SelectedItem);
                DateTime heure = Convert.ToDateTime(cbHeure.SelectedItem);
                DateTime dt = new DateTime(jour.Year, jour.Month, jour.Day, heure.Hour, heure.Minute, 0);
                //Verif qu'une activité ne commence pas à cette heure ci
                if (!PlanningDAO.ExistePlanning(dt.ToString("yyyy-MM-dd HH:mm:ss")))
                {
                    Activite a = (Activite)cbAct.SelectedItem;
                    DateTime dureeAct = Convert.ToDateTime(a.Duree);
                    DateTime dtJourHeure = new DateTime(jour.Year, jour.Month, jour.Day, 17, 0, 0);
                    //Soustraction d'une seconde pour que la date ne match pas en cas d'égalité 
                    //e.g : act de 10h a 11h et une act commencant a 11h
                    dureeAct = dureeAct.AddSeconds(-1);
                    //Verif qu'une activité ne sort pas du planning (heure max = 17h)
                    if (dt.Add(dureeAct.TimeOfDay) < dtJourHeure)
                    {
                        //Verif qu'une activité n'a pas lieu pendant la durée entrée
                        if (!PlanningDAO.EmpieteActiviteDebut(dt.ToString("yyyy-MM-dd HH:mm:ss"), dt.Add(dureeAct.TimeOfDay).ToString("yyyy-MM-dd HH:mm:ss")))
                        {
                            //Verif qu'une activité n'est pas déjà en cours 
                            if (!PlanningDAO.EmpieteSurActivite(dt.ToString("yyyy-MM-dd HH:mm:ss")))
                            {
                                Planning pAncien = (Planning)listeBoxPlanning.SelectedItem;
                                Activite aNew = (Activite)cbAct.SelectedItem;
                                Planning pNew = new Planning(dt, aNew);
                                PlanningDAO.ModifierPlanning(pAncien, pNew);
                                listeBoxPlanning.DataSource = null;
                                listeBoxPlanning.DataSource = PlanningDAO.GetPlanning(week.ToString("yyyy-MM-dd"), week.Add(new TimeSpan(4, 23, 59, 59)).ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                MessageBox.Show("Une activité est déjà en cours");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Cette activité empiète sur une activité se déroulant plus tard");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Une activité ne peut se terminer après 17h.");
                    }
                }
                else
                {
                    MessageBox.Show("Une activité commence déjà à cette heure");
                }
            }
            else
            {
                MessageBox.Show("Vous devez d'abord choisir une activité.");
            }
        }

        private void SetComboboxActSelectedItem(int idAct)
        {
            foreach (Activite a in cbAct.Items)
            {
                if (a.Id == idAct)
                {
                    cbAct.SelectedItem = a;
                }
            }
        }

        private void cbAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            Activite a = (Activite)cbAct.SelectedItem;
            lbDuree.Text = a.Duree.ToString();
            lbPart.Text = a.NbClient.ToString();
        }

        private void SetCbJours()
        {
            DateTime dtJours = week.Date;
            for (int i = 0; i < 5; i++)
            {
                cbJours.Items.Add(dtJours.ToString("D"));
                dtJours = dtJours.Add(new TimeSpan(1, 0, 0, 0));
            }
        }

        private void SetComboboxJoursSelectedItem(string duree)
        {
            foreach (string s in cbJours.Items)
            {
                if (s == duree)
                {
                    cbJours.SelectedItem = s;
                }
            }
        }

        private void SetCbHeures()
        {
            DateTime timeloop = new DateTime(0);
            timeloop = timeloop.Add(new TimeSpan(9, 00, 0)); //Commence à 9h 
            string s = "";
            for (int i = 0; i < 16; i++)
            {
                s = timeloop.ToString("HH:mm");
                cbHeure.Items.Add(s);
                timeloop = timeloop.Add(new TimeSpan(0, 30, 0)); //Ajout d'une demi-heure

            }
        }

        private void SetComboboxHeureSelectedItem(string duree)
        {
            foreach (string s in cbHeure.Items)
            {
                if (s == duree)
                {
                    cbHeure.SelectedItem = s;
                }
            }
        }

        private void btRetour_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btAjoutClient_Click(object sender, EventArgs e)
        {
            if (listeBoxPlanning.SelectedItem != null)
            {
                Planning p = (Planning)listeBoxPlanning.SelectedItem;
                this.Hide();
                v_ClientActivite cliAct = new v_ClientActivite(p);
                DialogResult dr = cliAct.ShowDialog();

                if (dr == DialogResult.Cancel)
                {
                    listeBoxPlanning.DataSource = null;
                    listeBoxPlanning.DataSource = PlanningDAO.GetPlanning(week.Date.ToString("yyyy-MM-dd"), week.Add(new TimeSpan(5, 23, 59, 59)).Date.ToString("yyyy-MM-dd"));
                    this.Show();
                }
            }
            else
            {
                MessageBox.Show("Vous devez d'abord choisir une activité du planning");
            }
        }

    }
}
