﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace PPE2
{
    static class EquipementDAO 
    {
        //Nom de la table
        private static string table = " equipement ";
        //Champs de la table
        private static string id = "cd_eq";
        private static string libelle = "libelle";
        private static string description = "description";
        private static string nombre = "nombre";
        private static int idEqParDefaut = 1; //Equipement utilisé par les activités sans équipement
        //Connexion à la bdd
        private static MySqlConnection conn = DB.Connexion();

        #region Create

        /// <summary>
        /// Ajout d'un équipement
        /// </summary>
        /// <param name="e">L'objet équipement</param>
        public static void InsererEquipement(Equipement e)
        {
            string req = "INSERT INTO " + table + " (" + id + "," + libelle + "," + description + "," + nombre + ")" +
                " VALUES(null, ?lib , ?descr , ?nb )";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?lib", e.Libelle);
            cmd.Parameters.AddWithValue("?descr", e.Description);
            cmd.Parameters.AddWithValue("?nb", e.Nombre);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        #endregion

        #region Read

        /// <summary>
        /// Liste des équipements triées par identifiant décroissant
        /// </summary>
        /// <returns></returns>
        public static List<Equipement> GetEquipements()
        {
            List<Equipement> listeEq = new List<Equipement>();
            string req = "SELECT * FROM" + table + "ORDER BY " + id + " DESC";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = req;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                listeEq.Add(new Equipement(rdr.GetInt32(id), rdr.GetString(libelle), rdr.GetString(description), rdr.GetInt32(nombre)));
            }
            rdr.Close();
            conn.Close();
            return listeEq;
        }

        /// <summary>
        /// Récupérer un équipement grâce à son identifiant
        /// </summary>
        /// <param name="idE">id de l'équipement</param>
        /// <returns></returns>
        public static Equipement GetEquipementsById(int idE)
        {
            List<Equipement> listeEq = new List<Equipement>();
            string req = "SELECT * FROM" + table + " WHERE " + id + "= ?id";
            conn.Open();
            Equipement e = null;
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?id", idE);
            cmd.CommandText = req;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                e = new Equipement(rdr.GetInt32(id), rdr.GetString(libelle), rdr.GetString(description), rdr.GetInt32(nombre));

            }
            rdr.Close();
            conn.Close();
            return e;
        }
        #endregion

        #region Update

        /// <summary>
        /// Modifier un équipement
        /// </summary>
        /// <param name="e">L'equipement à modifier</param>
        public static void ModifierEquipement(Equipement e)
        {
            if (e.Id != idEqParDefaut)
            {
                ActiviteDAO.ModifierNombreEq(e.Id, e.Nombre);
                string req = "UPDATE " + table + " SET " + description + "= ?description," +
                    nombre + "= ?nombre WHERE " + id + "=?id";
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.Parameters.AddWithValue("?description", e.Description);
                cmd.Parameters.AddWithValue("?nombre", e.Nombre);
                cmd.Parameters.AddWithValue("?id", e.Id);
                cmd.CommandText = req;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Impossible de modifier cet équipement"); 
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Supprimer un équipement
        /// </summary>
        /// <param name="e">L'équipement à supprimer</param>
        public static void SupprimerEquipement(Equipement e)
        {
            if (e.Id != idEqParDefaut)
            {
                ActiviteDAO.ModifierActivitesCodeEq(e.Id, idEqParDefaut);
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                string req = "DELETE FROM" + table + "WHERE " + id + " = ?id";
                cmd.Parameters.AddWithValue("?id", e.Id);
                cmd.CommandText = req;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Impossible de supprimer cet équipement");
            }
        }
        #endregion

        #region Existe

        /// <summary>
        /// Vérifie si l'equipement existe déjà
        /// </summary>
        /// <param name="l">Comparaison sur le libelle de l'equipement
        /// Insensible à la casse</param>
        /// <returns>true : existe
        /// false: n'existe pas</returns>
        public static bool ExisteEquipement(string l)
        {
            string reqEtus = "SELECT * from "+table+"  where lower("+libelle+") = ?l";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?l", l.ToLower());
            cmd.CommandText = reqEtus;
            MySqlDataReader rdrE = cmd.ExecuteReader();
            if (rdrE.Read())
            {
                rdrE.Close();
                conn.Close();
                return true;
            }
            rdrE.Close();
            conn.Close();
            return false;
        }
        #endregion
    }
}
