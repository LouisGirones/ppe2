﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace PPE2
{
    static class ActiviteDAO 
    {
        //Nom de la table
        private static string table = " activite ";
        //Champs de la table
        private static string id = "id_act";
        private static string nom = "nom";
        private static string description = "description";
        private static string duree = "duree_minutes";
        private static string nbClients = "nb_max_compte";
        private static string idEq= "cd_eq";
        //Connexion à la bdd
        private static MySqlConnection conn = DB.Connexion();

        #region Create

        /// <summary>
        /// Ajout d'une activité
        /// </summary>
        /// <param name="a">L'objet activité</param>
        /// <param name="e">L'objet équipement clef étrangère de la table activité pointant sur la table équipement</param>
        public static void InsererActivite(Activite a, Equipement e)
        {
            string req = "INSERT INTO " + table + " (" + id + "," + idEq + "," + nom + "," + description + "," + duree + "," + nbClients + ")" +
                " VALUES(null, ?idEq,?nom , ?descr , ?duree, ?nbCli )";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?idEq", e.Id);
            cmd.Parameters.AddWithValue("?nom", a.Nom);
            cmd.Parameters.AddWithValue("?descr", a.Description);
            cmd.Parameters.AddWithValue("?duree", a.Duree);
            cmd.Parameters.AddWithValue("?nbCli", a.NbClient);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        #endregion

        #region Read

        /// <summary>
        /// Renvoie la liste des activités triée par identifiant décroissant
        /// </summary>
        /// <returns></returns>
        public static List<Activite> GetActivites()
        {
            List<Activite> listeMat = new List<Activite>();
            string req = "SELECT * FROM" + table + "ORDER BY "+ id +" DESC";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = req;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                listeMat.Add(new Activite(rdr.GetInt32(id), rdr.GetInt32(idEq), rdr.GetString(nom), rdr.GetString(description), rdr.GetString(duree), rdr.GetInt32(nbClients)));
            }
            rdr.Close();
            conn.Close();
            return listeMat;
        }

        #endregion

        #region Update

        /// <summary>
        /// Modification d'une activité
        /// </summary>
        /// <param name="a">L'objet activité à modifier
        /// (l'id est celui de la base de données, les valeurs celles du formulaire)</param>
        public static void ModifierActivite(Activite a)
        {
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "UPDATE " + table + " SET " + description + "= ?description," +
               nbClients + " =?nbCli," + idEq + "=?idEq WHERE " + id + "=?id";
            cmd.Parameters.AddWithValue("?description", a.Description);
            cmd.Parameters.AddWithValue("?nombre", a.Duree);
            cmd.Parameters.AddWithValue("?nbCli", a.NbClient);
            cmd.Parameters.AddWithValue("?idEq", a.IdEq);
            cmd.Parameters.AddWithValue("?id", a.Id);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// Lorsqu'un équipement est supprimé on met l'équipement par défaut aux activités utilisant l'équipement supprimé
        /// </summary>
        /// <param name="eqSupprimer">Identifiant de l'équipement supprimé</param>
        /// <param name="eqDefaut">Identifiant de l'équipement par défaut</param>
        public static void ModifierActivitesCodeEq(int eqSupprimer, int eqDefaut)
        {
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "UPDATE " + table + " SET " + idEq + "=?idEqDef WHERE " + idEq + "=?idEqSup";
            cmd.Parameters.AddWithValue("?idEqSup", eqSupprimer);
            cmd.Parameters.AddWithValue("?idEqDef", eqDefaut);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// Lorsque le nombre d'un équipement est baissé, si le nombre max. de client d'une activité l'utilisant
        /// est inférieur au nouveau nombre, on met le nombre max. de client égal au nouveau nombre
        /// </summary>
        /// <param name="eqModif">Identifiant de l'équipement modifié</param>
        /// <param name="nbEq">Nouveau nombre d'équipement</param>
        public static void ModifierNombreEq(int eqModif, int nbEq)
        {
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "UPDATE " + table + " SET " + nbClients + "=?nbEq WHERE " + idEq + "=?idEqModif AND "+nbClients+"> ?nbEq";
            cmd.Parameters.AddWithValue("?idEqModif", eqModif);
            cmd.Parameters.AddWithValue("?nbEq", nbEq);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        #endregion

        #region Delete

        /// <summary>
        /// Suppression d'une activité
        /// </summary>
        /// <param name="a">L'activité à supprimer</param>
        public static void SupprimerActivite(Activite a)
        {
            //Suppression des participations avec cette activité
            ReserveDAO.SupprimerReservationsParAct(a.Id);
            //Suppression des plannings avec cette activité
            PlanningDAO.SupprimerPlanningParAct(a.Id);
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "DELETE FROM" + table + "WHERE " + id + " = ?id";
            cmd.Parameters.AddWithValue("?id", a.Id);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }


        #endregion

        #region Existe

        /// <summary>
        /// Verifie si l'activité existe déjà
        /// </summary>
        /// <param name="n">Comparaison sur le nom de l'activité. 
        /// Insensible à la casse</param>
        /// <returns>true : existe
        /// false: n'existe pas</returns>
        public static bool ExisteActivite(string n)
        {
            string reqEtus = "SELECT * from " + table + "  where lower(" + nom + ") = ?n";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?n", n.ToLower());
            cmd.CommandText = reqEtus;
            MySqlDataReader rdrE = cmd.ExecuteReader();
            if (rdrE.Read())
            {
                rdrE.Close();
                conn.Close();
                return true;
            }
            rdrE.Close();
            conn.Close();
            return false;
        }
        #endregion
    }
}
