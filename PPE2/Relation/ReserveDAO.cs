﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace PPE2
{
    static class ReserveDAO 
    {
        //Nom de la table
        private static string table = " participe ";
        //Champs de la table
        private static string idCompte = "id_compte";
        private static string idAct = "id_act";
        private static string date = "dt_hr_planning";
        //Connexion à la bdd
        private static MySqlConnection conn = DB.Connexion();

        #region Create

        /// <summary>
        /// Ajout d'un client à un planning
        /// </summary>
        /// <param name="c">Client</param>
        /// <param name="p">Planning</param>
        public static void InsererResa(Client c, Planning p)
        {
            string req = "INSERT INTO " + table + " (" + idCompte + "," + idAct + "," + date+ ")" +
                " VALUES(?idC, ?idA , ?dt)";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?idC", c.Id);
            cmd.Parameters.AddWithValue("?idA", p.Act.Id);
            cmd.Parameters.AddWithValue("?dt", p.DtJourHeureAct.ToString("yyyy-MM-dd HH:mm:ss"));
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        #endregion

        #region Read

        /// <summary>
        /// Renvoie la liste des clients participant à ce planning
        /// </summary>
        /// <param name="p">Planning</param>
        /// <returns></returns>
        public static List<Client> GetParticipants(Planning p)
        {
            List<Client> listeCli = new List<Client>();
            string req = "SELECT * FROM" + table +
                "JOIN compte ON compte.id_compte = participe.id_compte" +
                " WHERE " + idAct + "= ?idAct AND " + date + "=?dt";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?idAct", p.Act.Id);
            cmd.Parameters.AddWithValue("?dt", p.DtJourHeureAct.ToString("yyyy-MM-dd HH:mm:ss"));
            cmd.CommandText = req;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
               listeCli.Add(new Client(rdr.GetInt32("id_compte"), rdr.GetString("nom"), rdr.GetString("prenom"), rdr.GetString("sexe")));
            }
            rdr.Close();
            conn.Close();
            return listeCli;
        }

        /// <summary>
        /// Renvoie la liste des clients ne participant pas à ce planning
        /// </summary>
        /// <param name="p">Planning</param>
        /// <returns></returns>
        public static List<Client> GetClients(Planning p)
        {
            List<Client> listeCli = new List<Client>();
            string req = "SELECT * " +
                        "FROM   compte " +
                        "WHERE  NOT EXISTS (" +
                            "SELECT " + idCompte + " " +
                            "FROM  " + table + " "+
                            "WHERE  compte.ID_COMPTE = participe.id_compte " +
                            "AND participe.ID_ACT = ?idA " +
                            "AND participe.DT_HR_PLANNING = ?dt)";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?idA", p.Act.Id);
            cmd.Parameters.AddWithValue("?dt", p.DtJourHeureAct.ToString("yyyy-MM-dd HH:mm:ss"));
            cmd.CommandText = req;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                listeCli.Add(new Client(rdr.GetInt32("id_compte"), rdr.GetString("nom"), rdr.GetString("prenom"), rdr.GetString("sexe")));
            }
            rdr.Close();
            conn.Close();
            return listeCli;
        }

        /// <summary>
        /// Nombre de participant actuel d'un planning
        /// </summary>
        /// <param name="p">Planning</param>
        /// <returns></returns>
        public static int GetNbParticipantAct(Planning p)
        {
            string req = "SELECT COUNT(*) AS nb_part " +
                        "FROM" + table +
                        "WHERE " + idAct + " = ?idAct " +
                        "AND " + date + "=?date";
            int nbPart=0;
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?idAct", p.Act.Id);
            cmd.Parameters.AddWithValue("?date", p.DtJourHeureAct.ToString("yyyy-MM-dd HH:mm:ss"));
            cmd.CommandText = req;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                nbPart = rdr.GetInt32("nb_part");
            }
            rdr.Close();
            conn.Close();
            return nbPart;
        }
        #endregion

        #region Delete

        /// <summary>
        /// Suppression d'un client sur un planning
        /// </summary>
        /// <param name="c">Client</param>
        /// <param name="p">Planning</param>
        public static void SupprimerClient(Client c, Planning p)
        {
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "DELETE FROM" + table + "WHERE " + idCompte + " = ?idC "+
                "AND "+ idAct +"=?idA AND "+date+"=?dt";
            cmd.Parameters.AddWithValue("?idC", c.Id);
            cmd.Parameters.AddWithValue("?idA", p.Act.Id);
            cmd.Parameters.AddWithValue("?dt", p.DtJourHeureAct);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// Suppression de l'ensemble des clients du planning
        /// </summary>
        /// <param name="p">Planning</param>
        public static void SupprimerClients(Planning p)
        {
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "DELETE FROM" + table + "WHERE " +
                idAct + "=?idA AND " + date + "=?dt";
            cmd.Parameters.AddWithValue("?idA", p.Act.Id);
            cmd.Parameters.AddWithValue("?dt", p.DtJourHeureAct);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// Suppression de toutes les réservations concernant une activité
        /// </summary>
        /// <param name="id">Identifiant de l'activité</param>
        public static void SupprimerReservationsParAct(int id)
        {
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "DELETE FROM" + table + "WHERE " + idAct + " = ?idAct";
            cmd.Parameters.AddWithValue("?idAct", id);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// Suppression de tous les réservations concernant un client
        /// </summary>
        /// <param name="id">Identifiant du client</param>
        public static void SupprimerReservationsParClient(int id)
        {
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "DELETE FROM" + table + "WHERE " + idCompte + " = ?idC";
            cmd.Parameters.AddWithValue("?idC", id);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        #endregion

        #region Existe

        /// <summary>
        /// Le client participe déjà à ce plannning
        /// </summary>
        /// <param name="p">Planning</param>
        /// <param name="c">Client</param>
        /// <returns></returns>
        public static bool ExisteClientAct(Planning p, Client c)
        {
            string reqEtus = "SELECT * FROM "+table+"  WHERE "+ idCompte +" = ?idC AND "+ idAct + " = ?idA AND "+ date + "= ?dt";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?idC", c.Id);
            cmd.Parameters.AddWithValue("?idA", p.Act.Id);
            cmd.Parameters.AddWithValue("?dt", p.DtJourHeureAct);
            cmd.CommandText = reqEtus;
            MySqlDataReader rdrE = cmd.ExecuteReader();
            if (rdrE.Read())
            {
                rdrE.Close();
                conn.Close();
                return true;
            }
            rdrE.Close();
            conn.Close();
            return false;
        }
        #endregion
    }
}
