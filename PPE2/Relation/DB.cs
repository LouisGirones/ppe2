﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace PPE2
{
    static class DB
    {
        private static string serveur = "127.0.0.1"; //ip du serveur MySql
        private static string utilisateurBD = "root"; //Utilisateur
        private static string mdp = ""; //Mot de passe
        private static string nomBD = "vva2"; //Nom de la bdd
        private static string connexionDB = @"server="+serveur+";userid="+utilisateurBD+";password="+mdp+";database="+nomBD; //string de connexion
        private static MySqlConnection conn = null;

        /// <summary>
        /// Connexion à la base de données
        /// </summary>
        /// <returns>objet MySqlConnection</returns>
        public static MySqlConnection Connexion()
        {
            conn = new MySqlConnection(connexionDB);
            return conn;
        }

        //méthode de test du mysql connection dans le program.cs
    }
}
