﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace PPE2
{
    static class PlanningDAO
    {
        //Nom de la table
        private static string table = " planning ";
        //Champs de la table
        private static string idAct = "id_act";
        private static string date = "dt_hr_planning";
        //Connexion à la bdd
        private static MySqlConnection conn = DB.Connexion();

        #region Create

        /// <summary>
        /// Ajout d'un planning
        /// </summary>
        /// <param name="p">Le planning à ajouter</param>
        public static void InsererActivitePlanning(Planning p)
        {
            string req = "INSERT INTO " + table + " (" + idAct + "," + date + ")" +
                " VALUES(?idAct, ?date )";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?idAct", p.Act.Id);
            cmd.Parameters.AddWithValue("?date", p.DtJourHeureAct);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        #endregion

        #region Read

        /// <summary>
        /// Renvoie les planning de la semainde passée en paramètre
        /// </summary>
        /// <returns></returns>
        public static List<Planning> GetPlanning(string weekStart, string weekEnd)
        {
            List<Planning> listePlanning = new List<Planning>();
            string req = "SELECT * FROM" + table + " JOIN ACTIVITE ON ACTIVITE.ID_ACT = PLANNING.ID_ACT WHERE "+
            date +" BETWEEN ?dateDeb AND ?dateFin ORDER BY "+date;
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?dateDeb", weekStart);
            cmd.Parameters.AddWithValue("?dateFin", weekEnd);
            cmd.CommandText = req;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                Activite a = new Activite(rdr.GetInt32(idAct), rdr.GetInt32("cd_eq"), rdr.GetString("nom"), rdr.GetString("description"), rdr.GetString("duree_minutes"), rdr.GetInt32("nb_max_compte"));
                DateTime dt = Convert.ToDateTime(rdr[date]);
                listePlanning.Add(new Planning(dt, a));
            }
            rdr.Close();
            conn.Close();
            return listePlanning;
        }

        #endregion

        #region Update

        /// <summary>
        /// Modification d'un planning
        /// </summary>
        /// <param name="pAncien">L'ancien planning</param>
        /// <param name="pNouveau">Le nouveau planning</param>
        public static void ModifierPlanning(Planning pAncien, Planning pNouveau)
        {
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "UPDATE " + table + " SET " + idAct + "= ?idAct," +
               date + " =?date WHERE "+idAct+ "=?idActAncien AND "+date +" =?dateAncien";
            cmd.Parameters.AddWithValue("?idAct", pNouveau.Act.Id);
            cmd.Parameters.AddWithValue("?date", pNouveau.DtJourHeureAct);
            cmd.Parameters.AddWithValue("?idActAncien", pAncien.Act.Id);
            cmd.Parameters.AddWithValue("?dateAncien", pAncien.DtJourHeureAct);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        #endregion

        #region Delete

        /// <summary>
        /// Suppression d'un planning
        /// </summary>
        /// <param name="p">Le planning à supprimer</param>
        public static void SupprimerPlanning(Planning p)
        {
            //Suppression des réservations concernant ce planning
            ReserveDAO.SupprimerReservationsParAct(p.Act.Id);
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "DELETE FROM" + table + "WHERE " + idAct + " = ?idAct AND " + date + " = ?date";
            cmd.Parameters.AddWithValue("?idAct", p.Act.Id);
            cmd.Parameters.AddWithValue("?date", p.DtJourHeureAct);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// Supprimer des plannings selon leurs activités
        /// </summary>
        /// <param name="id">Identifiant de l'activité</param>
        public static void SupprimerPlanningParAct(int id)
        {
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "DELETE FROM" + table + "WHERE " + idAct + " = ?idAct";
            cmd.Parameters.AddWithValue("?idAct", id);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        #endregion

        #region Existe

        /// <summary>
        /// Verifie si un planning commence déjà à cette heure
        /// </summary>
        /// <param name="n">Date de début (yyyy-MM-dd HH:mm:ss)</param>
        /// <returns></returns>
        public static bool ExistePlanning(string d)
        {
            string reqEtus = "SELECT * from " + table + "  where " + date + " = ?date";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?date", d);
            cmd.CommandText = reqEtus;
            MySqlDataReader rdrE = cmd.ExecuteReader();
            if (rdrE.Read())
            {
                rdrE.Close();
                conn.Close();
                return true;
            }
            rdrE.Close();
            conn.Close();
            return false;
        }

        /// <summary>
        /// Vérifie si un planning commence entre une date de début et une date de fin
        /// </summary>
        /// <param name="dtDeb">Date de début</param>
        /// <param name="dtFin">Date de fin</param>
        /// <returns></returns>
        public static bool EmpieteActiviteDebut(string dtDeb, string dtFin)
        {
            string reqEtus = "SELECT * from " + table + "  where " + date + " BETWEEN ?dtDeb AND ?dtFin";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?dtDeb", dtDeb);
            cmd.Parameters.AddWithValue("?dtFin", dtFin);
            cmd.CommandText = reqEtus;
            MySqlDataReader rdrE = cmd.ExecuteReader();
            if (rdrE.Read())
            {
                rdrE.Close();
                conn.Close();
                return true;
            }
            rdrE.Close();
            conn.Close();
            return false;
        }

        /// <summary>
        /// Vérifie si un planning est déjà en cours à une date donnée
        /// </summary>
        /// <param name="dtDeb">Date</param>
        /// <returns></returns>
        public static bool EmpieteSurActivite(string dtDeb)
        {
            string req = "SELECT  p.DT_HR_PLANNING ," + //Date et heure de début du planning
                         "DATE_ADD("+
                         "      DATE_ADD(p.DT_HR_PLANNING, INTERVAL SUBSTRING(DUREE_MINUTES,1,2) HOUR),"+ //Ajout des heures de l'activité
                         "  INTERVAL SUBSTRING(DUREE_MINUTES,4,2) MINUTE"+ //Ajout des minutes de l'activité
                         ") as DT_HR_PLANNING_FIN " + //Date et heure de fin du planning
                         "FROM planning p " +
                         "JOIN activite a on a.ID_ACT = p.ID_ACT " +
                         "HAVING DT_HR_PLANNING < ?dtDeb " + // having pour utiliser les alias
                         "AND DT_HR_PLANNING_FIN > ?dtDeb";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?dtDeb", dtDeb);
            cmd.CommandText = req;
            MySqlDataReader rdrE = cmd.ExecuteReader();
            if (rdrE.Read())
            {
                rdrE.Close();
                conn.Close();
                return true;
            }
            rdrE.Close();
            conn.Close();
            return false;
        }

        #endregion
    }
}

