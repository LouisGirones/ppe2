﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace PPE2
{
    static class ClientDAO 
    {
        //Nom de la table
        private static string table = " compte ";
        //Champs de la table
        private static string id = "id_compte";
        private static string nom = "nom";
        private static string prenom = "prenom";
        private static string sexe = "sexe";
        //Connexion à la bdd
        private static MySqlConnection conn = DB.Connexion();

        #region Create

        /// <summary>
        /// Ajout d'un client
        /// </summary>
        /// <param name="c">L'objet client</param>
        public static void InsererClient(Client c)
        {
            string req = "INSERT INTO " + table + " (" + id + "," + nom + "," + prenom +"," + sexe+ ")" +
                " VALUES(null, ?nom , ?prenom, ?sexe)";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?nom", c.Nom);
            cmd.Parameters.AddWithValue("?prenom", c.Prenom);
            cmd.Parameters.AddWithValue("?sexe", c.Sexe);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        #endregion

        #region Read

        /// <summary>
        /// Renvoie la liste des clients triées par identifiant décroissant
        /// </summary>
        /// <returns></returns>
        public static List<Client> GetClients()
        {
            List<Client> listeCli = new List<Client>();
            string req = "SELECT * FROM" + table + "ORDER BY " + id + " DESC";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = req;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                listeCli.Add(new Client(rdr.GetInt32(id), rdr.GetString(nom), rdr.GetString(prenom), rdr.GetString(sexe)));
            }
            rdr.Close();
            conn.Close();
            return listeCli;
        }
        #endregion

        #region Update
        /// <summary>
        /// Modification d'un client
        /// </summary>
        /// <param name="c">Le client à modifier</param>
        public static void ModifierClient(Client c)
        {
                string req = "UPDATE " + table + " SET " +  nom + "= ?nom," +
                    prenom + "= ?prenom WHERE " + id + "=?id";
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.Parameters.AddWithValue("?prenom", c.Prenom);
                cmd.Parameters.AddWithValue("?nom", c.Nom);
                cmd.Parameters.AddWithValue("?id", c.Id);
                cmd.CommandText = req;
                cmd.ExecuteNonQuery();
                conn.Close();
        }
        #endregion

        #region Delete

        /// <summary>
        /// Suppression d'un client
        /// </summary>
        /// <param name="c">Le client à supprimer</param>
        public static void SupprimerClient(Client c)
        {
            ReserveDAO.SupprimerReservationsParClient(c.Id);
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            string req = "DELETE FROM" + table + "WHERE " + id + " = ?id";
            cmd.Parameters.AddWithValue("?id", c.Id);
            cmd.CommandText = req;
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        #endregion

        #region Existe

        /// <summary>
        /// Vérifie si le client existe déjà
        /// Comparaison sur le nom, prenom, sexe
        /// </summary>
        /// <param name="n">nom</param>
        /// <param name="p">prenom</param>
        /// <param name="s">sexe</param>
        /// <returns></returns>
        public static bool ExisteClient(string n, string p, string s)
        {
            string reqEtus = "SELECT * FROM "+table+"  WHERE lower("+ nom +") = ?nom AND lower("+ prenom + ") = ?prenom AND "+ sexe + "= ?sexe";
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("?prenom", p.ToLower());
            cmd.Parameters.AddWithValue("?nom", n.ToLower());
            cmd.Parameters.AddWithValue("?sexe", s);
            cmd.CommandText = reqEtus;
            MySqlDataReader rdrE = cmd.ExecuteReader();
            if (rdrE.Read())
            {
                rdrE.Close();
                conn.Close();
                return true;
            }
            rdrE.Close();
            conn.Close();
            return false;
        }
        #endregion
    }
}
