﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PPE2
{
    static class Program
    {
        public static v_Equipement v_Eq;
        public static v_Activite v_Act;
        public static v_Client v_Cli;
        public static v_Planning v_Plan;
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            v_Eq = new v_Equipement();
            v_Act = new v_Activite();
            v_Cli = new v_Client();
            v_Plan = new v_Planning();
            Application.Run(v_Plan);
        }
    }
}
