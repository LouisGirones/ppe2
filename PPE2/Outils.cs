﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PPE2
{
    static class Outils
    {
        /// <summary>
        /// Si la chaine est supérieure aux nombre de charactère, on la coupe au nombre de char. et y ajoute "..."
        /// </summary>
        /// <param name="chaine">Chaîne</param>
        /// <param name="nbChar">Nombre de char.</param>
        /// <returns></returns>
        public static string WordWrap(string chaine, int nbChar)
        {
            if (chaine.Length > nbChar)
            {
                chaine = chaine.Substring(0, nbChar-1);
                chaine += "...";
            }
            return chaine;
        }

        /// <summary>
        /// Vide les textboxs d'un form
        /// </summary>
        /// <param name="f">La form où vider les tb</param>
        public static void ViderTextBoxs(Form f)
        {
            foreach (Control c in f.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    ((TextBox)(c)).Text = string.Empty;
                }
            }
        }

        /// <summary>
        /// Fenetre modal de confirmation (yes/no)
        /// Commence par "Etes vous sur de vouloir "
        /// </summary>
        /// <param name="msg">message décrivant l'action à confirmer</param>
        /// <returns></returns>
        public static bool ConfirmationModal(string msg)
        {
            DialogResult dialogResult = MessageBox.Show("Etes vous sur de vouloir " + msg, "Confirmation", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Civilité du client
        /// </summary>
        /// <param name="sexe">Homme ou Femme</param>
        /// <param name="h">La valeur si Homme</param>
        /// <param name="f">La valeur si Femme</param>
        /// <returns></returns>
        public static string CiviliteClient(string sexe, string h, string f)
        {
            return sexe == "Homme" ? h : f;
        }
    }
}
