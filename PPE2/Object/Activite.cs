﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPE2
{
    public class Activite
    {
        //Champs
        private int id;
        private int idEq;
        private string nom;
        private string description;
        private string duree;
        private int nbClient;

        #region Constructeurs

        /// <summary>
        /// Constructeur d'un objet activité à utiliser lorsque l'on connait l'identifiant de l'activité
        /// </summary>
        /// <param name="id">Identifiant</param>
        /// <param name="idEq">Identifiant de l'équipement utilisé</param>
        /// <param name="nom">Nom</param>
        /// <param name="description">Description</param>
        /// <param name="duree">Durée</param>
        /// <param name="nbClient">Nombre de client max.</param>
        public Activite(int id, int idEq, string nom, string description, string duree, int nbClient)
        {
            this.id = id;
            this.idEq = idEq;
            this.nom = nom;
            this.description = description;
            this.duree = duree;
            this.nbClient = nbClient;
        }

        /// <summary>
        /// Constructeur d'un objet activité 
        /// </summary>
        /// <param name="idEq">Identifiant de l'équipement utilisé par l'activité</param>
        /// <param name="nom">Nom de l'activité</param>
        /// <param name="description">Description de l'activité</param>
        /// <param name="duree">Durée de l'activité</param>
        /// <param name="nbClient">Nombre de client max. de l'activité</param>
        public Activite(int idEq, string nom, string description, string duree, int nbClient)
        {
            this.id = 0;
            this.idEq = idEq;
            this.nom = nom;
            this.description = description;
            this.duree = duree;
            this.nbClient = nbClient;
        }

        #endregion

        #region Proprietes

        public int Id
        {
            get { return id; }
        }
        public int IdEq
        {
            get { return idEq; }
            set { idEq = value; }
        }
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string Duree
        {
            get { return duree; }
            set { duree = value; }
        }

        public int NbClient
        {
            get { return nbClient; }
            set { nbClient = value; }
        }
        #endregion

        #region ToString
        public override string ToString()
        {
            string act = "";
            string d = this.duree.Replace(":", "h")+"mn";
            act = this.nom +" -- "+d+" -- "+this.nbClient+" places totales";
            return act;
        }
        #endregion
    }
}
