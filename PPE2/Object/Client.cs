﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPE2
{
    public class Client
    {
        //Champs
        private int id;
        private string nom;
        private string prenom;
        private string sexe;

        #region Constructeurs

        /// <summary>
        /// Constructeur d'un objet client à utiliser lorsque l'on connait l'identifiant du client
        /// </summary>
        /// <param name="id">Identifiant</param>
        /// <param name="nom">Nom</param>
        /// <param name="prenom">Prénom</param>
        /// <param name="sexe">Sexe</param>
        public Client(int id, string nom, string prenom, string sexe)
        {
            this.id = id;
            this.nom = nom;
            this.prenom = prenom;
            this.sexe = sexe;
        }

        /// <summary>
        /// Constructeur d'un objet client
        /// </summary>
        /// <param name="nom">Nom</param>
        /// <param name="prenom">Prénom</param>
        /// <param name="sexe">Sexe</param>
        public Client( string nom, string prenom, string sexe)
        {
            this.id = 1;
            this.nom = nom;
            this.prenom = prenom;
            this.sexe = sexe;
        }
        #endregion

        #region Proprietes
        public int Id
        {
            get { return id; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }
        public string Sexe
        {
            get { return sexe; }
            set { sexe = value; }
        }
        #endregion

        #region ToString
        public override string ToString()
        {
            string cli = "";            
            cli =  Outils.CiviliteClient(this.sexe, "M","Mme")+" "+this.nom + " " + this.prenom;
            return cli;
        }
        #endregion
    }
}
