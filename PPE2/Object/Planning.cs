﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPE2
{
    public class Planning
    {
        //Champs
        private DateTime dtJourHeureAct;
        private Activite act;

        #region Constructeurs

        /// <summary>
        /// Constructeur d'un objet planning
        /// </summary>
        /// <param name="dt">Date de début de l'activité</param>
        /// <param name="a">Activité</param>
        public Planning(DateTime dt, Activite a)
        {
            this.dtJourHeureAct = dt;
            this.act = a;
        }

        #endregion

        #region Propriétés



        public DateTime DtJourHeureAct
        {
            get { return dtJourHeureAct; }
            set { dtJourHeureAct = value; }
        }

        public Activite Act
        {
            get { return act; }
            set { act = value; }
        }

        #endregion

        #region ToString
        public override string ToString()
        {
            //On convertit la durée de l'activité en DateTime
            DateTime dureeAct = Convert.ToDateTime(this.act.Duree);
            string planning = "";
            planning = this.DtJourHeureAct.ToString("dd/MM") + " " + this.act.Nom + " " + //jour mois + nom
                dtJourHeureAct.ToString("HH:mm") + "-" + //heure de début
                dtJourHeureAct.Add(new TimeSpan(dureeAct.Hour, dureeAct.Minute, 0)).ToString("HH:mm")+" "+ //heure de fin
                ReserveDAO.GetNbParticipantAct(this).ToString() + "/" + this.act.NbClient.ToString() + //Nb participant / nb max participant
                " participants";
            return planning;
        }
        #endregion

    }
}
