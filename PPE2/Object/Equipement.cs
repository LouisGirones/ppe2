﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPE2
{
    public class Equipement
    {
        //Champs
        private int id;
        private string libelle;
        private string description;
        private int nombre;

        #region Constructeurs

        /// <summary>
        /// Constructeur d'un objet équipement à utiliser lorsque l'on connait l'identifiant de l'équipement
        /// </summary>
        /// <param name="id">Identifiant</param>
        /// <param name="libelle">Libelle</param>
        /// <param name="description">Description</param>
        /// <param name="nombre">Nombre</param>
        public Equipement(int id, string libelle, string description, int nombre)
        {
            this.id = id;
            this.libelle = libelle;
            this.description = description;
            this.nombre = nombre;
        }

        /// <summary>
        /// Constructeur d'un objet équipement
        /// </summary>
        /// <param name="libelle">Libelle</param>
        /// <param name="description">Description</param>
        /// <param name="nombre">Nombre</param>
        public Equipement( string libelle, string description, int nombre)
        {
            this.id = 1;
            this.libelle = libelle;
            this.description = description;
            this.nombre = nombre;
        }

        #endregion

        #region Proprietes
        public int Id
        {
            get { return id; }
        }

        public string Libelle
        {
            get { return libelle; }
            set { libelle = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public int Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            string mat = "";
            mat =  this.libelle + " -- " + Outils.WordWrap(this.description, 20) + " -- " + this.nombre+ " Dispo.";
            return mat;
        }

        #endregion
    }
}
