﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PPE2
{
    static class Verification
    {
        /// <summary>
        /// L'entier est supérieur à zéro
        /// </summary>
        /// <param name="i">l'entier à vérifier</param>
        /// <returns>true >0
        /// false inferieur ou égal à 0</returns>
        public static bool SuperieurAZero(int i)
        {
            return i > 0;
        }

        /// <summary>
        /// Le string n'est pas vide ou composé d'espaces
        /// </summary>
        /// <param name="s">Le string n'est pas vide (ou espace)</param>
        /// <returns>true si le string est vide</returns>
        public static bool IsEmpty(string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }

        public static bool VerifNbPartNbEq(int nbPart, int nbEq)
        {
            return nbEq >= nbPart;
        }

        /// <summary>
        /// Un radio bouton est selectionné
        /// </summary>
        /// <param name="f">formulaire ayant les radios boutons</param>
        /// <returns>true si un radio bouton est sélectionné</returns>
        public static bool SelectedRb(Form f)
        {
            foreach (RadioButton rb in f.Controls.OfType<RadioButton>())
            {
                if (rb.Checked == true)
                {
                    return true;
                } 
            }
            return false;
        }

    }
}
