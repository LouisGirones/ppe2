﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace PPE2
{
    static class OutilsAffichagePlanning
    {
        //Nombre deux rangées (2 par heure)
        private static int nbRows = 16;

        //Titre des colonnes
        private static string[] headersNom = { "Horaire", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi" };

        /// <summary>
        /// Méthode d'affichage du datagridview
        /// </summary>
        /// <param name="dtg">Le datagridview dans lequel afficher les données</param>
        /// <param name="lPlann">La liste des plannings de la semaine</param>
        /// <param name="week">Le premier jour de la semaine</param>
        public static void Initialize(DataGridView dtg, List<Planning> lPlann, DateTime week)
        {
            //Vider le datagridview
            dtg.Rows.Clear();
            dtg.Refresh();

            // Nombre de colonne
            dtg.ColumnCount = headersNom.Count();
            dtg.ColumnHeadersVisible = true;

            //Désactiver le tri des colonnes
            dtg.Columns.Cast<DataGridViewColumn>().ToList().
                ForEach(f => f.SortMode = DataGridViewColumnSortMode.NotSortable);

            //Permettre le retour à la ligne dans les cellules
            DataGridViewTextBoxColumn dtgTbCol = new DataGridViewTextBoxColumn();
            dtgTbCol.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dtg.DefaultCellStyle = dtgTbCol.DefaultCellStyle;

            //Hauteur minimal d'une rangée de cellule
            dtg.RowTemplate.MinimumHeight = 40;
           
            //Masquer la colonne ajouter automatiquement par le contrôle datagridview
            dtg.RowHeadersVisible = false;

            //Nom des colonnes 
            SetColonnesPlanning(dtg, week);

            //Genération des rangées
            CreerPlanningVide(dtg, week);
            if (lPlann.Count > 0) //Il existe au moins un planning pour cette semaine
            {
                RemplirPlanning(dtg, lPlann);
            }
        }

        /// <summary>
        /// Remplir le nom de chaque colonne du datagridview selon la date
        /// </summary>
        /// <param name="dtg">Datagridview</param>
        /// <param name="week">Date du lundi de la semaine</param>
        private static void SetColonnesPlanning(DataGridView dtg, DateTime week)
        {
            // Nom des colonnes
            dtg.Columns[0].Name = headersNom[0];
            for (int i = 1; i < headersNom.Count(); i++)
            {
                dtg.Columns[i].Name = headersNom[i] + " " + week.Date.ToString("dd/MM/yy");
                //Ajout d'un jour à chaque itération
                week = week.Add(new TimeSpan(1, 0, 0, 0));
            }
        }

        /// <summary>
        /// Créer les rangées de 9 à 17h.
        /// </summary>
        /// <param name="dtg">Datagridview</param>
        /// <param name="week">Date du lundi de la semaine</param>
        private static void CreerPlanningVide(DataGridView dtg, DateTime week)
        {
            // Creating DateTimes
            DateTime heure = new DateTime(0);
            DateTime jour = new DateTime(0);
            //Setting hour value
            heure = heure.Add(new TimeSpan(9, 00, 0));
            //Setting day value for tag
            jour = jour.AddYears(week.Year - 1).AddMonths(week.Month - 1).AddDays(week.Day - 1).AddHours(9);
            for (int i = 0; i < nbRows; i++)
            {
                dtg.Rows.Add();
                //Colonne des horaires
                dtg[0, i].Value = heure.ToString("HH:mm") + " - ";
                heure = heure.Add(new TimeSpan(0, 30, 0));
                dtg[0, i].Value += heure.ToString("HH:mm");
                //On parcours les cellules pour leur donner le tag correspondant à la date
                for (int j = 1; j < (headersNom.Count()); j++)
                {
                    //Affectation du tag à la cellule
                    dtg[j, i].Tag = jour.Add(new TimeSpan(j - 1, 0, 0, 0)).ToString("yyyy-MM-dd HH:mm:ss");
                }
                jour = jour.Add(new TimeSpan(0, 30, 0));
            }
        }

        /// <summary>
        /// Affichages des activité de la semaine
        /// </summary>
        /// <param name="dtg">Le datagridview</param>
        /// <param name="lPlann">La liste des activités de la semaine</param>
        private static void RemplirPlanning(DataGridView dtg, List<Planning> lPlann)
        {
            DateTime dtDeb;
            DateTime dtFin;
            DateTime dureeAct;
            int compteur;
            //Parcours des plannings
            foreach (Planning p in lPlann)
            {
                dtDeb = p.DtJourHeureAct; //date de debut du planning
                //Parcours des rows du datagridview
                foreach (DataGridViewRow row in dtg.Rows)
                {
                    //Parcours des cellules du row
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        //Si le tag de la cellule correspond a l'heure de début de l'activité sur le planning
                        if ((string)cell.Tag == p.DtJourHeureAct.ToString("yyyy-MM-dd HH:mm:ss"))
                        {
                            dureeAct = Convert.ToDateTime(p.Act.Duree); 
                            dtFin = dtDeb.Add(dureeAct.TimeOfDay);
                            compteur = 0;
                            //Tant que la date de debut de ce planning est inferieur a sa date de fin
                            while (dtDeb < dtFin)
                            {
                                //Affichage du contenu de la cellule
                                if (dtDeb == p.DtJourHeureAct) //debut activité
                                { 
                                    dtg[cell.ColumnIndex, cell.RowIndex + compteur].Value = p.Act.Nom+
                                        Environment.NewLine + ReserveDAO.GetNbParticipantAct(p).ToString() + "/" +
                                        p.Act.NbClient + " participants";
                                    dtg[cell.ColumnIndex, cell.RowIndex + compteur].Style.BackColor = Color.Green;
                                }
                                else
                                    if (dtDeb.Add(new TimeSpan(0, 30, 0)) < dtFin) //poursuite activité
                                    {
                                        dtg[cell.ColumnIndex, cell.RowIndex + compteur].Value = p.Act.Nom + " (suite)";
                                        dtg[cell.ColumnIndex, cell.RowIndex + compteur].Style.BackColor = Color.Gray;
                                    }
                                    else // fin activité
                                    {
                                        dtg[cell.ColumnIndex, cell.RowIndex + compteur].Value = p.Act.Nom + " (fin)";
                                        dtg[cell.ColumnIndex, cell.RowIndex + compteur].Style.BackColor = Color.Red;
                                    }
                                dtDeb = dtDeb.Add(new TimeSpan(0, 30, 0));
                                compteur += 1;

                            }

                        }
                    }
                }

            }

        }
    }
}
