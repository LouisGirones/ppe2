-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 26 Avril 2016 à 22:07
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `vva2`
--

-- --------------------------------------------------------

--
-- Structure de la table `activite`
--

DROP TABLE IF EXISTS `activite`;
CREATE TABLE IF NOT EXISTS `activite` (
  `ID_ACT` int(11) NOT NULL AUTO_INCREMENT,
  `CD_EQ` int(11) DEFAULT NULL,
  `NOM` char(32) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION` char(200) COLLATE utf8_bin DEFAULT NULL,
  `DUREE_MINUTES` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `NB_MAX_COMPTE` int(2) DEFAULT NULL,
  PRIMARY KEY (`ID_ACT`),
  KEY `CD_EQ` (`CD_EQ`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `activite`
--

INSERT INTO `activite` (`ID_ACT`, `CD_EQ`, `NOM`, `DESCRIPTION`, `DUREE_MINUTES`, `NB_MAX_COMPTE`) VALUES
(33, 1, 'Test', 'Test', '01:30', 3),
(34, 1, 'Test2', 'Test', '00:30', 5),
(35, 1, 'Test3', 'Test', '02:30', 9);

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

DROP TABLE IF EXISTS `compte`;
CREATE TABLE IF NOT EXISTS `compte` (
  `ID_COMPTE` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` char(32) COLLATE utf8_bin DEFAULT NULL,
  `PRENOM` char(32) COLLATE utf8_bin DEFAULT NULL,
  `SEXE` enum('Homme','Femme') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID_COMPTE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `equipement`
--

DROP TABLE IF EXISTS `equipement`;
CREATE TABLE IF NOT EXISTS `equipement` (
  `CD_EQ` int(11) NOT NULL AUTO_INCREMENT,
  `LIBELLE` char(32) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION` char(200) COLLATE utf8_bin DEFAULT NULL,
  `NOMBRE` int(6) DEFAULT NULL,
  PRIMARY KEY (`CD_EQ`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `equipement`
--

INSERT INTO `equipement` (`CD_EQ`, `LIBELLE`, `DESCRIPTION`, `NOMBRE`) VALUES
(1, 'Aucun', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `participe`
--

DROP TABLE IF EXISTS `participe`;
CREATE TABLE IF NOT EXISTS `participe` (
  `ID_COMPTE` int(11) NOT NULL,
  `ID_ACT` int(11) NOT NULL,
  `DT_HR_PLANNING` datetime NOT NULL,
  PRIMARY KEY (`ID_COMPTE`,`ID_ACT`,`DT_HR_PLANNING`),
  KEY `FK_PARTICIPE_PLANNING` (`ID_ACT`,`DT_HR_PLANNING`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `planning`
--

DROP TABLE IF EXISTS `planning`;
CREATE TABLE IF NOT EXISTS `planning` (
  `ID_ACT` int(11) NOT NULL,
  `DT_HR_PLANNING` datetime NOT NULL,
  PRIMARY KEY (`ID_ACT`,`DT_HR_PLANNING`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `planning`
--

INSERT INTO `planning` (`ID_ACT`, `DT_HR_PLANNING`) VALUES
(33, '2016-04-22 09:00:00'),
(33, '2016-04-22 10:30:00'),
(34, '2016-04-21 14:30:00'),
(35, '2016-03-14 09:00:00'),
(35, '2016-04-11 09:00:00'),
(35, '2016-04-11 12:00:00'),
(35, '2016-04-18 15:00:00'),
(35, '2016-04-18 16:00:00'),
(35, '2016-04-19 09:00:00'),
(35, '2016-04-21 10:00:00'),
(35, '2016-04-25 09:00:00'),
(35, '2016-04-25 15:00:00');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `activite`
--
ALTER TABLE `activite`
  ADD CONSTRAINT `activite_ibfk_1` FOREIGN KEY (`CD_EQ`) REFERENCES `equipement` (`CD_EQ`);

--
-- Contraintes pour la table `participe`
--
ALTER TABLE `participe`
  ADD CONSTRAINT `participe_ibfk_1` FOREIGN KEY (`ID_COMPTE`) REFERENCES `compte` (`ID_COMPTE`),
  ADD CONSTRAINT `participe_ibfk_2` FOREIGN KEY (`ID_ACT`,`DT_HR_PLANNING`) REFERENCES `planning` (`ID_ACT`, `DT_HR_PLANNING`);

--
-- Contraintes pour la table `planning`
--
ALTER TABLE `planning`
  ADD CONSTRAINT `planning_ibfk_1` FOREIGN KEY (`ID_ACT`) REFERENCES `activite` (`ID_ACT`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
